#pragma once
#ifndef RENDERER_DLL_GRAPHICS_SYSTEM_H
#define RENDERER_DLL_GRAPHICS_SYSTEM_H

#include <Include\Utillities_Dll\Settings.h>
#include <Include\Utillities_Dll\Console.h>
#include <Include\Utillities_Dll\Timer.h>

#include <Include\Renderer_Dll\Direct3D.h>

class DLL_EXPORT_IMPORT CRenderer
{
public:
	CRenderer(HWND hWnd, CTimer * pTimer, CSettings * pSettings, CConsole * pConsole);
	~CRenderer();

	bool Init(HWND hWnd);
	void Resize();
	void Update();
	void Draw();
	void Shutdown();

	CDirect3D * const GetDirect3D() const;

private:
	CRenderer();

	bool m_bIsInitialized;

	CTimer * m_pSystemTimer;
	CSettings * m_pSystemSettings;
	CConsole * m_pMainConsole;

	HWND m_hWnd;

	CDirect3D * m_pDirect3D;
};

#endif