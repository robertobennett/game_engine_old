#pragma once
#ifndef _RENDERER_DLL_I_GEOMETRY_H_
#define _RENDERER_DLL_I_GEOMETRY_H_

#include <d3d11.h>
#include <dxgi.h>
#include <dxdiag.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

#include <Include\Renderer_Dll\Interfaces\IShader.h>

class DLL_EXPORT_IMPORT IGeometry
{
public:
	IGeometry();
	~IGeometry();

	virtual bool Create();
	virtual void Update();
	virtual void Draw();
	virtual void Destroy();

	XMFLOAT4X4 GetWorldMatrix() const;
	IShader * GetBoundShader() const;
	ID3D11Buffer * const * D3D11GetVertexBuffer() const;
	UINT GetVertBuffStride() const;
	UINT GetVertBuffOffset() const;
	ID3D11Buffer * D3D11GetIndexBuffer() const;
	UINT GetIndexBuffOffset() const;
	UINT GetVertexCount() const;
	UINT GetIndicieCount() const;
	ID3D11RasterizerState * D3D11GetRS() const;

protected:
	void CalculateFaceNormal(XMFLOAT3 *xmf3A, XMFLOAT3 *xmf3B, XMFLOAT3 *xmf3C, XMFLOAT3 *xmf3Out);
	void CalculateFaceNormal(XMFLOAT3 *xmf3A, XMFLOAT3 *xmf3B, XMFLOAT3 *xmf3C, XMVECTOR &xmvOut);

	XMFLOAT4X4 m_xmf4x4WorldMatrix;

	UINT m_iVertexCount;
	UINT m_iIndicieCount;
	UINT m_iVertexBufferStride;
	UINT m_iVertexBufferOffset;
	UINT m_iIndexBufferOffset;

	ID3D11Buffer * m_pD3D11VertexBuffer;
	ID3D11Buffer * m_pD3D11IndexBuffer;
	ID3D11RasterizerState * m_pD3D11RS;

	IShader * m_pBoundShader;
};

#endif