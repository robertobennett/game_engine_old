#pragma once
#ifndef _RENDERER_DLL_I_CAMERA_H_
#define _RENDERER_DLL_I_CAMERA_H_

#include <DirectXMath.h>

#include <Include\Utillities_Dll\Settings.h>
#include <Include\Utillities_Dll\Console.h>
#include <Include\Utillities_Dll\Timer.h>

using namespace DirectX;

class DLL_EXPORT_IMPORT ICamera
{
public:
	ICamera(HWND hWnd, CTimer* pTimer, CSettings * pSettings, CConsole * pConsole);
	~ICamera();

	virtual bool Create();
	virtual void Update();
	virtual void Shutdown();

	XMFLOAT4X4 GetViewMatrix() const;
	XMFLOAT4X4 GetProjectionMatrix() const;

protected:
	HWND m_hMainWnd;

	CTimer * m_pSystemTimer;
	CSettings * m_pSystemSettings;
	CConsole * m_pMainConsole;

	XMVECTOR m_xmvUp;
	XMFLOAT4X4 m_xmf4x4View;
	XMFLOAT4X4 m_xmf4x4Proj;

private:
	ICamera();
};

#endif