#pragma once
#ifndef _RENDERER_DLL_I_SHADER_H_
#define _RENDERER_DLL_I_SHADER_H_

#include <d3d11.h>
#include <dxgi.h>
#include <dxdiag.h>
#include <d3dcompiler.h>

#include <DirectXMath.h>

using namespace DirectX;

class DLL_EXPORT_IMPORT IShader
{
public:
	IShader();
	~IShader();

	virtual bool Create();
	virtual void Update();
	virtual void Draw();
	virtual void Destroy();

	virtual void UpdateConstantBuffers(XMFLOAT4X4 xmf4x4World, XMFLOAT4X4 xmf4x4View, XMFLOAT4X4 xmf4x4Proj);

	D3D11_PRIMITIVE_TOPOLOGY D3D11GetTopology() const;
	ID3D11InputLayout * D3D11GetInputLayout() const;
	ID3D11VertexShader * D3D11GetVertexShader() const;
	ID3D11PixelShader * D3D11GetPixelShader() const;
	ID3D11Buffer * const * D3D11GetConstantBuffer() const;

protected:
	std::wstringstream m_wssShaderPath;

	D3D11_PRIMITIVE_TOPOLOGY m_D3D11PrimitiveTopology;
	ID3D11InputLayout * m_pD3D11InputLayout;
	ID3D11VertexShader * m_pD3D11VertexShader;
	ID3D11PixelShader * m_pD3D11PixelShader;
	ID3D11Buffer * m_pD3D11ConstantBuffer;
};

#endif