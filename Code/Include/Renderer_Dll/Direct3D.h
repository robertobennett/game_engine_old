#pragma once
#ifndef RENDERER_DLL_DIRECT_3D_H
#define RENDERER_DLL_DIRECT_3D_H

#include <d3d11.h>
#include <dxgi.h>
#include <dxdiag.h>
#include <d3dcompiler.h>
#include <DirectXMath.h>

#include <Include\Utillities_Dll\Settings.h>
#include <Include\Utillities_Dll\Console.h>
#include <Include\Utillities_Dll\Timer.h>

#include <Include\Renderer_Dll\Interfaces\IGeometry.h>
#include <Include\Renderer_Dll\Interfaces\ICamera.h>

using namespace DirectX;

#ifdef _DEBUG
#define D3D_CREATE_DEVICE_FLAGS D3D11_CREATE_DEVICE_DEBUG
#else
#define D3D_CREATE_DEVICE_FLAGS NULL
#endif

class DLL_EXPORT_IMPORT CDirect3D
{
public:
	CDirect3D(HWND hWnd, CTimer * pTimer, CSettings * pSettings, CConsole * pConsole);
	~CDirect3D();
	
	bool Init(HWND hWnd);
	void Resize();
	void Update();
	void Draw();
	void Shutdown();

	void ResetRasterState();
	bool SetRasterState(ID3D11RasterizerState * pRS);

	bool BindCamera(ICamera * pCamera);

	bool RegisterGeometry(IGeometry * pGeom);
	bool UnregisterGeometry(IGeometry * pGeom);

	HRESULT D3D11CreateInputLayout(D3D11_INPUT_ELEMENT_DESC * pDesc, UINT iElementCount, const void * pShaderBytecode, SIZE_T iByteCodeSize, ID3D11InputLayout** pLayout);
	HRESULT D3D11CreatePixelShader(const void * pShaderBytecode, SIZE_T iBytecodeSize, ID3D11ClassLinkage * pClsLnk, ID3D11PixelShader ** pPixelShader);
	HRESULT D3D11CreateVertexShader(const void * pShaderBytecode, SIZE_T iBytecodeSize, ID3D11ClassLinkage * pClsLnk, ID3D11VertexShader ** pVertexShader);
	HRESULT D3D11CreateBuffer(D3D11_BUFFER_DESC * pBuffDesc, D3D11_SUBRESOURCE_DATA * pInitData, ID3D11Buffer ** pBuffer);
	HRESULT D3D11Map(ID3D11Resource * pResource, UINT iSubresource, D3D11_MAP mapType, UINT iMapFlags, D3D11_MAPPED_SUBRESOURCE * pMappedResource);
	bool D3D11Unmap(ID3D11Resource * pResource, UINT iSubresource);

	ID3D11RasterizerState * GetRSDefault() const;
	ID3D11RasterizerState * GetRSDefaultNoCull() const;
	ID3D11RasterizerState * GetRSWireFrameCullBack() const;
	ID3D11RasterizerState * GetRSWireFrameNoCull() const;
private:
	CDirect3D();

	bool m_bInitialized, m_bMSAAEnabled, m_bRasterStateOverrode;;
	int m_iWidth, m_iHeight;
	UINT m_iMSAASampleCount, m_iMSAASampleQuality;

	CTimer * m_pSystemTimer;
	CSettings * m_pSystemSettings;
	CConsole * m_pMainConsole;

	HWND m_hWnd;

	ID3D11Debug * m_pD3D11Debug;
	ID3D11Device * m_pD3D11Device;
	ID3D11DeviceContext * m_pD3D11DeviceContext;
	IDXGISwapChain * m_pDXGISwapChain;
	ID3D11RenderTargetView * m_pD3D11RenderTarget;
	ID3D11Texture2D * m_pD3D11DepthStencilBuffer;
	ID3D11DepthStencilView * m_pD3D11DepthStencilView;
	ID3D11RasterizerState * m_pD3D11RSDefault;
	ID3D11RasterizerState * m_pD3D11RSDefaultNoCull;
	ID3D11RasterizerState * m_pD3D11RSWireFrameCullBackface;
	ID3D11RasterizerState * m_pD3D11RSWireFrameNoCull;

	ICamera * m_pBoundCamera;
	std::vector <IGeometry*> m_pRegisteredGeometry;
};

#endif