#pragma once
#ifndef _RENDERER_DLL_STDAFX_H_
#define _RENDERER_DLL_STDAFX_H_

#include <Windows.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm> 

#ifdef _DLL_EXPORT_
#define DLL_EXPORT_IMPORT __declspec(dllexport)
#else
#define DLL_EXPORT_IMPORT __declspec(dllimport)
#endif

#define SafeDelete(x) { if(x) { delete x; x = NULL; }}

//Longest possible NTFS filepath ? http://stackoverflow.com/questions/2825798/should-i-deal-with-files-longer-than-max-path
#define LONGEST_FILE_PATH 32767

//Path to the Game_Engine folder
const std::string GetRootFolderPath();
const std::wstring wGetRootFolderPath();

#endif