#ifndef _ENGINE_EXE_STDAFX_H_
#define _ENGINE_EXE_STDAFX_H_

#include <Windows.h>
#include <stdlib.h>
#include <string>
#include <vector>
#include <fstream>
#include <sstream>
#include <algorithm> 

#ifdef _DLL_EXPORT_
#define DLL_EXPORT_IMPORT __declspec(dllexport)
#else
#define DLL_EXPORT_IMPORT __declspec(dllimport)
#endif

#define SafeDelete(x) { if(x) { delete x; x = NULL; } }

#endif