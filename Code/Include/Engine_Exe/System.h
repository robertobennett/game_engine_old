#pragma once
#ifndef ENGINE_EXE_SYSTEM_H
#define ENGINE_EXE_SYSTEM_H

#include <Include\Utillities_Dll\Timer.h>
#include <Include\Utillities_Dll\Settings.h>
#include <Include\Utillities_Dll\IEventListener.h>
#include <Include\Utillities_Dll\EventHandler.h>

#include <Include\Engine_Dll\Engine.h>

#include <Include\Engine_Exe\MainWindow.h>

class CSystem : public IEventListener
{
public:
	CSystem();
	~CSystem();

	bool Init();
	void Run();
	void Shutdown();

	virtual void WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

private:
	bool m_bIsInitialized;
	bool m_bIsEditor;
	bool m_bIsPaused;

	CTimer * m_pSystemTimer;
	CSettings * m_pSystemSettings;
	CMainWindow * m_pMainWindow;
	CConsole * m_pMainConsole;

	CEngine * m_pEngine;
	//CGame * m_pGame;
};

#endif