#pragma once
#ifndef _ENGINE_MAIN_WINDOW_H_
#define _ENGINE_MAIN_WINDOW_H_

#include <Include\Utillities_Dll\EventHandler.h>
#include <Include\Utillities_Dll\IWindow.h>
#include <Include\Utillities_Dll\Settings.h>

enum WINDOW_DISP_MODES {
	WDM_WINDOWED,
	WDM_FULLSCREEN_BORDERLESS
};

class CMainWindow : public IWindow, public IEventListener
{
public:
	~CMainWindow();

	virtual bool Create();
	virtual void Resize();
	virtual void Update();
	virtual void Draw();
	virtual void Destroy();

	virtual HWND GetHWND();
	virtual int GetWidth();
	virtual int GetHeight();

	/////////////////////////////////////////
	virtual bool RegisterChildWindow(IWindow* pWnd) { return IWindow::RegisterChildWindow(pWnd); }
	virtual bool UnregisterChildWindow(IWindow* pWnd) { return IWindow::RegisterChildWindow(pWnd); }
	////////////////////////////////////////
	
	virtual void WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

	static CMainWindow* GetInstance();

private:
	CMainWindow();
	WINDOW_DISP_MODES m_WndDispMode;
	CSettings * m_pSettings;
	static CMainWindow* m_pInstance;
};

#endif