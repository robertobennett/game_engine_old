#pragma once
#ifndef ENGINE_DLL_TEST_CAMERA_H
#define ENGINE_DLL_TEST_CAMERA_H

#include <Include\Utillities_Dll\IEventListener.h>
#include <Include\Utillities_Dll\EventHandler.h>

#include <Include\Renderer_Dll\Interfaces\ICamera.h>

#include <Include\Engine_Dll\Geometry\Grid.h>

class CEngineCamera : public ICamera, public IEventListener
{
public:
	CEngineCamera(HWND hWnd, CTimer * pTimer, CSettings * pSettings, CConsole * pConsole);
	~CEngineCamera();

	virtual bool Create();
	virtual void Update();
	virtual void Shutdown();

	virtual void MouseEvent(HWND hWnd, float fX, float fY, float fDeltaX, float fDeltaY, bool bLeftClick, bool bRightClick);
	virtual void WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

private:
	CEngineCamera();

	bool m_bCreated;
	
	double m_fCameraSpeed, m_fCameraRotationSpeedRad, m_fFOV, m_fNear, m_fFar;
	XMVECTOR m_xmvCameraPos;
	XMVECTOR m_xmvCameraRotRad;
	XMVECTOR m_xmvCameraTarget;

	XMVECTOR CalculateCameraTarget();
};

#endif