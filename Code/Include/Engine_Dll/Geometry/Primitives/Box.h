#pragma once
#ifndef _ENGINE_DLL_BOX_H_
#define _ENGINE_DLL_BOX_H_

#include <Include\Engine_Dll\GlobalDefs.h>

#include <Include\Renderer_Dll\Interfaces\IGeometry.h>
#include <Include\Renderer_Dll\Renderer.h>
#include <Include\Renderer_Dll\Interfaces\IShader.h>

class CBox : public IGeometry
{
public:
	CBox(CRenderer * pRenderer, IShader * pShader, XMFLOAT4 pos);
	~CBox();

	virtual bool Create();
	virtual void Update();
	virtual void Draw();
	virtual void Destroy();

private:
	bool m_bCreated;

	CRenderer * m_pRenderer;

	XMFLOAT4 m_xmf4Pos;
};

#endif