#pragma once
#ifndef ENGINE_DLL_TERRAIN_H
#define ENGINE_DLL_TERRAIN_H

#include <Include\Engine_Dll\GlobalDefs.h>

#include <Include\Renderer_Dll\Renderer.h>
#include <Include\Renderer_Dll\Interfaces\IGeometry.h>

#include <Include\Utillities_Dll\FileManager.h>

class CTerrain : public IGeometry
{
public:
	CTerrain(std::string sName, std::string sPath, CRenderer * pRend, IShader * pShader);
	~CTerrain();

	virtual bool Create();
	virtual void Destroy();

private:
	struct HeightMapPointDesc
	{
		float x, y, z;
	};

	bool m_bCreated;
	int m_iMaxHeight, m_iWidth, m_iDepth;
	std::vector<Data*> m_Data;
	std::vector<HeightMapPointDesc> m_HeightMapDesc;
	std::vector<GlobalDefs::SimpleVertex> m_Verts;
	std::vector<unsigned int> m_Indicies;

	inline bool LoadTerrainSettings();
	bool LoadTerrainBitmap();
	inline bool CreateGeometry();

	float MapHeight(float fIn);

	CTerrain();	
	std::string m_sTerrainName;
	std::string m_sTerrainFolderPath;

	CRenderer * m_pRenderer;
	CFileManager * m_pFileManager;
};

#endif