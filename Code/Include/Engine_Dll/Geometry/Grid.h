#pragma once
#ifndef _ENGINE_DLL_GRID_H_
#define _ENGINE_DLL_GRID_H_

#include <Include\Renderer_Dll\Renderer.h>
#include <Include\Renderer_Dll\Interfaces\IGeometry.h>
#include <Include\Engine_Dll\GlobalDefs.h>

class CGrid : public IGeometry
{
public:
	CGrid(CRenderer * pRenderer, IShader * pShader, XMFLOAT4 pos, int iWidth, int iDepth, bool bVertGrid);
	~CGrid();

	virtual bool Create();
	virtual void Update();
	virtual void Draw();
	virtual void Destroy();

private:
	bool m_bCreated;
	bool m_bVerticalGrid;
	std::vector<GlobalDefs::SimpleVertex> m_Verts;
	std::vector<UINT> m_Inicies;

	XMFLOAT4 m_xmf4Pos;
	int m_iWidth, m_iDepth;

	CRenderer * m_pRenderer;
};

#endif