#pragma once
#ifndef _RENDERER_H_BASIC_SHADER_H_
#define _RENDERER_H_BASIC_SHADER_H_

#include <Include\Engine_Dll\GlobalDefs.h>

#include <Include\Renderer_Dll\Renderer.h>
#include <Include\Renderer_Dll\Interfaces\IShader.h>

using namespace DirectX;

class CBasicShader : public IShader
{
public:
	CBasicShader(CRenderer * pRenderer);
	~CBasicShader();

	virtual bool Create();
	virtual void Update();
	virtual void Draw();
	virtual void Destroy();

	virtual void UpdateConstantBuffers(XMFLOAT4X4 xmf4x4World, XMFLOAT4X4 xmf4x4View, XMFLOAT4X4 xmf4x4Proj);

private:
	bool m_bCreated;

	CRenderer * m_pRenderer;
};

#endif