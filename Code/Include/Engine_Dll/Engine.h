#pragma once
#ifndef ENGINE_DLL_SYSTEM_H
#define ENGINE_DLL_SYSTEM_H

////////////////////
#include <Include\Engine_Dll\Shaders\BasicShader.h>
#include <Include\Engine_Dll\Cameras\EngineCamera.h>
#include <Include\Engine_Dll\Geometry\Primitives\Box.h>
#include <Include\Engine_Dll\Geometry\Terrain.h>
/////////////////

#include <Include\Utillities_Dll\Settings.h>
#include <Include\Utillities_Dll\Console.h>
#include <Include\Utillities_Dll\Timer.h>

#include <Include\Renderer_Dll\Renderer.h>

#include <Include\Engine_Dll\ConsoleFunctions.h>

class DLL_EXPORT_IMPORT CEngine : public IEventListener
{
public:
	CEngine(HWND hWnd, CTimer * pTimer, CSettings * pSettings, CConsole * pConsole);
	~CEngine();

	bool Init();
	void Update();
	void Resize();
	void Shutdown();

private:
	CEngine();

	bool m_bIsInitialized;
	HWND m_hMainWnd;

	CTimer * m_pSystemTimer;
	CSettings * m_pSystemSettings;
	CConsole * m_pMainConsole;
	CRenderer * m_pRenderer;
	CConsoleFunctions * m_pConsoleFunctions;

	////////////////////////////
	CBasicShader * m_pBsSh;
	CEngineCamera * m_pTC;
	CTerrain * m_pTerrain;
	////////////////////////////
};

#endif