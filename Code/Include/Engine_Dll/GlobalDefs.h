#pragma once
#ifndef _ENGINE_DLL_GLOBAL_DEFS_H_
#define _ENGINE_DLL_GLOBAL_DEFS_H_

#include <DirectXMath.h>

namespace GlobalDefs
{
	struct SimpleVertex
	{
		DirectX::XMFLOAT3 xmf3Pos;
		DirectX::XMFLOAT3 xmf3Norm;
		DirectX::XMFLOAT4 xmf4Colour;
	};

	struct MatrixBuffer
	{
		DirectX::XMFLOAT4X4 xmf4x4World;
		DirectX::XMFLOAT4X4 xmf4x4View;
		DirectX::XMFLOAT4X4 xmf4x4Proj;
	};
}

#endif