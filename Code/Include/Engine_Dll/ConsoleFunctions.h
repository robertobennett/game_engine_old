#pragma once
#ifndef ENGINE_DLL_CONSOLE_FUNCTIONS_H
#define ENGINE_DLL_CONSOLE_FUNCTIONS_H

#include <Include\Renderer_Dll\Renderer.h>

#include <Include\Utillities_Dll\Console.h>

class CConsoleFunctions
{
public:
	~CConsoleFunctions();

	bool RegisterFunctions();
	bool UnregisterFunctions();

	static void Exit();
	static void ResetRasterState();
	static void SetRasterModeWireframe();
	static void SetRasterModeWireframeNoCull();
	static void SetRasterModeDefault();

	static CConsoleFunctions * GetInstance();
	static CConsoleFunctions * GetInstance(HWND hWnd, CConsole* pConsole, CRenderer* pRenderer);

private:
	CConsoleFunctions();
	CConsoleFunctions(HWND hWnd, CConsole* pConsole, CRenderer* pRenderer);

	HWND m_hMainWindow;
	CConsole* m_pMainConsole;
	CRenderer* m_pRenderer;

	static CConsoleFunctions * m_pInstance;
};

#endif