#pragma once
#ifndef _ENGINE_EVENT_HANDLER_H_
#define _ENGINE_EVENT_HANDLER_H_

#include <Include\Utillities_Dll\EventHandler.h>
#include <Include\Utillities_Dll\IEventListener.h>

#include <windowsx.h>

class CEventHandler
{
public:
	DLL_EXPORT_IMPORT static CEventHandler* GetInstance();
	DLL_EXPORT_IMPORT static LRESULT CALLBACK WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

	DLL_EXPORT_IMPORT bool RegisterListener(IEventListener* pListener);
	DLL_EXPORT_IMPORT bool UnregisterListener(IEventListener* pListener);
	
	DLL_EXPORT_IMPORT void PassWindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

private:
	CEventHandler();
	~CEventHandler();

	float m_fMouseX, m_fMouseY, m_fMousePrevX, m_fMousePrevY;

	static CEventHandler* m_pInstance;

	std::vector<IEventListener*> m_pEventListeners;
};

#endif