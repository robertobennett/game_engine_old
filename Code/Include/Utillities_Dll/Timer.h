#pragma once
#ifndef _UTILLITIES_DLL_TIMER_H_
#define _UTILLITIES_DLL_TIMER_H_

class DLL_EXPORT_IMPORT CTimer {
public:
	CTimer();
	~CTimer();

	void Start();
	void Tick();
	void Pause();
	void Unpause();
	void TogglePause();
	void Reset();
	void Stop();

	int GetTimeElapsed();
	double GetDeltaT();

private:
	bool m_bStarted;
	bool m_bPaused;
	
	__int64 m_iTicksPerSec, m_iStartPauseTicks, m_iAcumelatedPausedTicks, m_iStartTicks, m_iCurrentTicks, m_iPreviousTicks;
};

#endif