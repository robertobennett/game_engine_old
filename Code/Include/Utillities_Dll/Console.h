#pragma once
#ifndef _ENGINE_CONSOLE_H_
#define _ENGINE_CONSOLE_H_

#include <Include\Utillities_Dll\Settings.h>
#include <Include\Utillities_Dll\IWindow.h>

class CConsole : public IWindow, public IEventListener
{
public:
	DLL_EXPORT_IMPORT CConsole();
	DLL_EXPORT_IMPORT ~CConsole();

	DLL_EXPORT_IMPORT virtual bool Create(HWND hParentWnd);
	DLL_EXPORT_IMPORT virtual void Resize();
	DLL_EXPORT_IMPORT virtual void Update();
	DLL_EXPORT_IMPORT virtual void Draw();
	DLL_EXPORT_IMPORT virtual void Destroy();

	DLL_EXPORT_IMPORT virtual void WriteToConsole(std::string sOut);
	DLL_EXPORT_IMPORT virtual void AddToInput(const char cCharacter);

	DLL_EXPORT_IMPORT virtual void CallConsoleFunction(std::string sName);
	DLL_EXPORT_IMPORT virtual bool RegisterConsoleFunction(std::string sName, void(*pFunc)(), std::string sDesc = "No description !");
	DLL_EXPORT_IMPORT virtual bool UnregisterConsoleFunction(std::string sName);

	DLL_EXPORT_IMPORT virtual void SetConsoleVariable(std::string sName, std::string sValue);
	DLL_EXPORT_IMPORT virtual bool RetriveConsoleVariable(std::string sName);
	DLL_EXPORT_IMPORT virtual bool RegisterConsoleVariable(std::string sName, void* pValue, std::string sDesc = "No description !");
	DLL_EXPORT_IMPORT virtual bool UnregisterConsoleVariable(std::string sName);

	DLL_EXPORT_IMPORT virtual HWND GetHWND();
	DLL_EXPORT_IMPORT virtual int GetWidth();
	DLL_EXPORT_IMPORT virtual int GetHeight();

	/////////////////////////////////////////////////
	DLL_EXPORT_IMPORT virtual bool RegisterChildWindow(IWindow* pWnd) { return IWindow::RegisterChildWindow(pWnd); }
	DLL_EXPORT_IMPORT virtual bool UnregisterChildWindow(IWindow* pWnd) { return IWindow::RegisterChildWindow(pWnd); }
	////////////////////////////////////////////////

	DLL_EXPORT_IMPORT virtual void WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);

private:
	struct ConsoleFunctions 
	{
		std::string sName;
		std::string sDesc;
		void (*pFunc)();
	};

	struct ConsoleVariables 
	{
		std::string sName;
		std::string sDesc;
		void* pData; //Store it as void, cast it as type based off of name !
	};

	inline void UpdateScrollBar();
	inline void FormatLine(std::string sLine);
	inline void ReformatConsoleOutput();
	inline void UpdateScrollPos(int iOffset);
	inline void ParaseInput();
	inline void PrintConsoleHelp();
	inline void NavigateCommandHistory(int iOffset);

	bool m_bVisable, m_bCapitalize;
	int m_iOutputWidth, m_iCommandHistoryPos;
	unsigned int m_iMaxLinesToDraw, m_iScrollPos;
	double m_dHeightScaleFactor, m_dWidthScaleFactor;

	TEXTMETRIC * m_pFontData;

	std::stringstream m_ssInput;
	std::vector<std::string> m_sRawLines, m_sLinesToDraw, m_sCommandHistory;
	std::vector<ConsoleFunctions> m_ConsoleFunctions;
	std::vector<ConsoleVariables> m_ConsoleVariables;

	CSettings * m_pSettings;
};

#endif