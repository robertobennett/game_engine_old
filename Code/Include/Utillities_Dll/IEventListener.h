#pragma once
#ifndef _ENGINE_INTERFACE_EVENT_LISTENER_H_
#define _ENGINE_INTERFACE_EVENT_LISTENER_H_

class DLL_EXPORT_IMPORT IEventListener
{
public:
	//virtual void MouseTrack(float fX, float fY, float fDeltaX, float fDeltaY);
	//On right click or left click it sends message on movement with which button is pressed
	//It also passes the change in x and y between when key is pressed and the mesage sent
	virtual void MouseEvent(HWND hWnd, float fX, float fY, float fDeltaX, float fDeltaY, bool bLeftClick, bool bRightClick);
	//virtual void KeyEvent(KEY_CODE key, bool bDown);
	virtual void WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam);
};

#endif