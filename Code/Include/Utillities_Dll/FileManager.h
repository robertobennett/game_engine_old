#pragma once
#ifndef _FILE_MANAGER_H_
#define _FILE_MANAGER_H_

#include <fstream>

struct Data
{
	std::string sName;
	std::string sClassification;
	std::string sValue;
};

struct Classification
{
	std::string sName;
	int iNumOfData;
	int iLineBounds[2];
};

class CFileManager
{
public:
	DLL_EXPORT_IMPORT CFileManager();
	DLL_EXPORT_IMPORT ~CFileManager();

	DLL_EXPORT_IMPORT bool Create(std::string sFilePath);
	DLL_EXPORT_IMPORT bool ReadFile();
	DLL_EXPORT_IMPORT std::vector<Data*> GetData() const;
	DLL_EXPORT_IMPORT bool UpdateData(std::vector<Data*>& data);
	DLL_EXPORT_IMPORT void Destroy();

	DLL_EXPORT_IMPORT void DumpToDebug();

private:
	std::fstream m_fsFile;
	std::string m_sFilePath;
	std::vector<char*> m_pLines;
	std::vector<Classification*> m_Classifications;
	std::vector<Data*> m_Data;

	int m_iDumpNumber;

	inline bool ReadLines();
	inline bool ReadClassifications();
	inline bool ReadData();
};

#endif

//<classification>
//	iData = 0
//	fData = 1.0
//	bData = true
//</classification>