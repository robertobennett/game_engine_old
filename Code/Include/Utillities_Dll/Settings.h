#pragma once
#ifndef _ENGINE_DLL_SETTINGS_H_
#define _ENGINE_DLL_SETTINGS_H_

#include <Include\Utillities_Dll\FileManager.h>

union VarReturnType {
	bool b;
	unsigned int i;
	double d;
};

//Make a singleton
class CSettings
{
public:
	DLL_EXPORT_IMPORT ~CSettings();

	DLL_EXPORT_IMPORT static CSettings* GetInstance();

	DLL_EXPORT_IMPORT void SaveSettings();
	DLL_EXPORT_IMPORT void SetVar(std::string sClassificationName, std::string sVarName, bool bVarValue);
	DLL_EXPORT_IMPORT void SetVar(std::string sClassificationName, std::string sVarName, int iVarValue);
	DLL_EXPORT_IMPORT void SetVar(std::string sClassificationName, std::string sVarName, unsigned int iVarValue);
	DLL_EXPORT_IMPORT void SetVar(std::string sClassificationName, std::string sVarName, double dVarValue);
	DLL_EXPORT_IMPORT VarReturnType GetVar(std::string sClassificationName, std::string sVarName);

private:
	CSettings();

	inline void Init();

	CFileManager * m_pFileManager;
	std::vector<Data*> m_pData;

	static std::string m_s_sFilePath;
	static CSettings* m_s_pInstance;
};

#endif