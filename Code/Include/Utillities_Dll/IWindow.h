#pragma once
#ifndef _ENGINE_DLL_INTERFACE_WINDOW_H_
#define _ENGINE_DLL_INTERFACE_WINDOW_H_

#include <Include\Utillities_Dll\EventHandler.h>

class IWindow
{
public:
	virtual ~IWindow() {
		return;
	}

	DLL_EXPORT_IMPORT virtual bool Create();
	DLL_EXPORT_IMPORT virtual bool Create(HWND hWndParent);
	DLL_EXPORT_IMPORT virtual void Resize();
	DLL_EXPORT_IMPORT virtual void Update();
	DLL_EXPORT_IMPORT virtual void Draw();
	DLL_EXPORT_IMPORT virtual void Destroy();

	DLL_EXPORT_IMPORT virtual HWND GetHWND();
	DLL_EXPORT_IMPORT virtual int GetWidth();
	DLL_EXPORT_IMPORT virtual int GetHeight();

	////////////////////////////
	DLL_EXPORT_IMPORT virtual bool RegisterChildWindow(IWindow* pWindow);
	DLL_EXPORT_IMPORT virtual bool UnregisterChildWindow(IWindow* pWindow);
	////////////////////////////

	DLL_EXPORT_IMPORT WNDPROC GetWndProcHandle();

protected:
	HWND m_hWnd, m_hWndParent;
	//IWindow * m_pParent;
	bool m_bVisable, m_bMaximized, m_bMinimized;
	int m_iWidth, m_iHeight, m_iXPos, m_iYPos;

	///////////////////////////
	std::vector<IWindow*> m_ChildWindows;
	/////////////////////////////

	static WNDPROC WindowProcedure;
	static CEventHandler * m_pEventHandler;
};

#endif