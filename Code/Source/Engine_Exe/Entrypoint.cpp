#include <Include\Engine_Exe\stdafx.h>
#include <Include\Engine_Exe\System.h>

int WINAPI wWinMain(HINSTANCE hCurrInstance, HINSTANCE hPrevInstance, PWSTR sCmdLine, int iCmdShow)
{
	CSystem * pSystem = new CSystem();
	if (pSystem->Init())
		pSystem->Run();

	pSystem->Shutdown();
	SafeDelete(pSystem);

	return 0;
}