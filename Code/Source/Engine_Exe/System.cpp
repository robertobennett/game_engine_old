#include <Include\Engine_Exe\stdafx.h>
#include <Include\Engine_Exe\System.h>

CSystem::CSystem():
m_bIsInitialized(false),
m_bIsEditor(true),
m_bIsPaused(false),
m_pSystemTimer(nullptr),
m_pSystemSettings(nullptr),
m_pMainWindow(nullptr),
m_pMainConsole(nullptr),
m_pEngine(nullptr)
{
}

CSystem::~CSystem()
{
}

void CSystem::WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	if (m_pMainWindow->GetHWND() == hWnd)
	{
		switch (iMsg)
		{
			default:
				break;

			case WM_ENTERSIZEMOVE:
			{
				m_pSystemTimer->Pause();
				m_bIsPaused = true;
				break;
			}

			case WM_ACTIVATE:
			{
				m_pSystemTimer->Unpause();
				m_bIsPaused = false;
				break;
			}

			case WM_SIZE:
			{
				if (wParam == SIZE_MINIMIZED)
				{
					m_bIsPaused = true;
					break;
				}
				else if (wParam == SIZE_RESTORED)
				{
					m_pMainWindow->Resize();
					m_bIsPaused = false;
					break;
				}

				break;
			}

			case WM_MOVE:
				m_pMainWindow->Resize();
				break;

			case WM_EXITSIZEMOVE:
			{
				m_pSystemTimer->Unpause();
				m_pMainWindow->Resize();
				m_pEngine->Resize();
				m_bIsPaused = false;
				break;
			}
		}

		return;
	}

	return;
}

bool CSystem::Init()
{
	if (!m_bIsInitialized)
	{
		m_pSystemTimer = new CTimer();
		m_pSystemTimer->Start();
		
		m_pSystemSettings = CSettings::GetInstance();

		m_pMainWindow = CMainWindow::GetInstance();
		m_pMainConsole = new CConsole();
		if (!m_pMainWindow->Create())
			return false;

		if (!m_pMainConsole->Create(m_pMainWindow->GetHWND()))
			return false;

		m_pMainWindow->RegisterChildWindow(m_pMainConsole);

		m_pEngine = new CEngine(m_pMainWindow->GetHWND(), m_pSystemTimer, m_pSystemSettings, m_pMainConsole);
		if (!m_pEngine->Init())
			return false;

		SetCapture(m_pMainWindow->GetHWND());
		SetActiveWindow(HWND_DESKTOP);
		SetActiveWindow(m_pMainWindow->GetHWND());

		CEventHandler::GetInstance()->RegisterListener(this);

		m_bIsInitialized = true;
		return true;
	}

	return false;
}

void CSystem::Run()
{
	if (m_bIsInitialized)
	{
		MSG msg; ZeroMemory(&msg, sizeof(msg));
		while (msg.message != WM_QUIT)
		{
			if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
			else
			{
				m_pSystemTimer->Tick();

				m_pMainWindow->Update();
				m_pMainWindow->Draw();

				if (!m_bIsPaused)
				{
					if (m_bIsEditor)
						m_pEngine->Update();
					else
					{
						//m_pGame->Update();
					}
				}

				//Demo timer with CFunc
				/*m_pMainConsole->WriteToConsole("Time Elapsed (s)");
				m_pMainConsole->WriteToConsole(std::to_string(m_pSysTimer->GetTimeElapsed()));
				m_pMainConsole->WriteToConsole("Delta T (s)");
				m_pMainConsole->WriteToConsole(std::to_string(m_pSysTimer->GetDeltaT()));*/
			}
		}

		return;
	}

	return;
}

void CSystem::Shutdown()
{
	if (m_pEngine)
		m_pEngine->Shutdown();
	SafeDelete(m_pEngine);

	ReleaseCapture();
	if (m_pMainWindow)
		m_pMainWindow->Destroy();
	SafeDelete(m_pMainConsole);
	CEventHandler::GetInstance()->UnregisterListener(this);
	SafeDelete(m_pMainWindow);

	//Settings doesnt need deleting...
	SafeDelete(m_pSystemTimer);

	return;
}
