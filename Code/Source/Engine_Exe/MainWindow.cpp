#include <Include\Engine_Dll\stdafx.h>
#include <Include\Engine_Exe\MainWindow.h>

CMainWindow* CMainWindow::m_pInstance = new CMainWindow();

CMainWindow::CMainWindow()
{
	//Standard set up of params
	m_hWnd = NULL;
	m_hWndParent = NULL;

	m_bVisable = true;
	m_bMaximized = true;
	m_bMinimized = false;

	m_iHeight = 800;
	m_iWidth = 600;
	m_iXPos = 0;
	m_iYPos = 0;
	m_WndDispMode = WDM_WINDOWED;

	return;
}

CMainWindow::~CMainWindow()
{
	return;
}

bool CMainWindow::Create()
{
	m_pSettings = CSettings::GetInstance();

	m_bMaximized = m_pSettings->GetVar("WindowParams", "bMaximized").b;
	m_iHeight = m_pSettings->GetVar("WindowParams", "iHeight").i;
	m_iWidth = m_pSettings->GetVar("WindowParams", "iWidth").i;
	m_iXPos = m_pSettings->GetVar("WindowParams", "iXPos").i;
	m_iYPos = m_pSettings->GetVar("WindowParams", "iYPos").i;
	m_WndDispMode = (WINDOW_DISP_MODES)(m_pSettings->GetVar("WindowParams", "iDispMode").i);

	//Create window class
	WNDCLASSEX wndClsEx; ZeroMemory(&wndClsEx, sizeof(wndClsEx));
	wndClsEx.cbSize = sizeof(wndClsEx);
	wndClsEx.hbrBackground = (HBRUSH)(COLOR_WINDOW);
	wndClsEx.hCursor = LoadIcon(GetModuleHandle(NULL), IDC_ARROW);
	wndClsEx.hInstance = GetModuleHandle(NULL);
	wndClsEx.lpfnWndProc = GetWndProcHandle();
	wndClsEx.lpszClassName = L"stdwnd";
	wndClsEx.style = CS_VREDRAW | CS_HREDRAW;

	//Register window class
	if (FAILED(RegisterClassEx(&wndClsEx)))
	{
		return false;
	}

	//Check display mode and create window accordingly
	if (m_WndDispMode == WDM_WINDOWED)
	{
		m_hWnd = CreateWindowA("stdwnd", "Engine", WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_VISIBLE, m_iXPos, m_iYPos, m_iWidth, m_iHeight, NULL, NULL, GetModuleHandle(NULL), NULL);
	}
	else if (m_WndDispMode == WDM_FULLSCREEN_BORDERLESS)
	{
		m_hWnd = CreateWindowA("stdwnd", "Engine", WS_POPUP | WS_CLIPCHILDREN | WS_VISIBLE, m_iXPos, m_iYPos, m_iWidth, m_iHeight, NULL, NULL, GetModuleHandle(NULL), NULL);
	}

	//Check if window was created
	if (!m_hWnd)
		return false;

	//Register this class to recive events
	if (!CEventHandler::GetInstance()->RegisterListener(this))
		return false;

	//Check if it should be maximized
	if (m_bMaximized)
	{
		ShowWindow(m_hWnd, SW_MAXIMIZE);
	}

	return true;
}

void CMainWindow::Resize()
{
	//Update vars
	RECT rc; ZeroMemory(&rc, sizeof(rc));
	GetWindowRect(m_hWnd, &rc);
	m_iWidth = rc.right - rc.left;
	m_iHeight = rc.bottom - rc.top;
	m_iXPos = rc.left;
	m_iYPos = rc.top;

	//Call this same function but for all child windows
	if (m_ChildWindows.size() > 0)
	{
		for (UINT i = 0; i < m_ChildWindows.size(); i++)
		{
			m_ChildWindows[i]->Resize();
		}
	}

	return;
}

void CMainWindow::Update()
{
	//Call this same function but for all child windows
	if (m_ChildWindows.size() > 0)
	{
		for (UINT i = 0; i < m_ChildWindows.size(); i++)
		{
			m_ChildWindows[i]->Update();
		}
	}

	return;
}

void CMainWindow::Draw()
{
	//Call this same function but for all child windows
	if (m_ChildWindows.size() > 0)
	{
		for (UINT i = 0; i < m_ChildWindows.size(); i++)
		{
			m_ChildWindows[i]->Draw();
		}
	}

	return;
}

void CMainWindow::Destroy()
{
	//Unregister all if any child windows
	if (m_ChildWindows.size() > 0)
	{
		for (UINT i = 0; i < m_ChildWindows.size(); i++)
		{
			UnregisterChildWindow(m_ChildWindows[i]);
		}
	}

	m_pSettings->SetVar("WindowParams", "bMaximized", m_bMaximized);
	m_pSettings->SetVar("WindowParams", "iHeight", m_iHeight);
	m_pSettings->SetVar("WindowParams", "iWidth", m_iWidth);
	m_pSettings->SetVar("WindowParams", "iXPos", m_iXPos);
	m_pSettings->SetVar("WindowParams", "iYPos", m_iYPos);
	m_pSettings->SetVar("WindowParams", "iDispMode", m_WndDispMode);

	m_pSettings->SaveSettings();

	//Deregister the event listener
	CEventHandler::GetInstance()->UnregisterListener(this);

	return;
}

void CMainWindow::WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	if (hWnd == m_hWnd)
	{
		switch (iMsg)
		{
		case WM_SIZE:
		{
			if (wParam == SIZE_MAXIMIZED)
			{
				m_bMaximized = true;
				break;
			}
			else if (wParam == SIZE_MINIMIZED)
			{
				m_bMinimized = true;
				break;
			}
			else if (wParam == SIZE_RESTORED)
			{
				if (m_bMinimized)
				{
					m_bMinimized = false;
					break;
				}
				else
				{
					m_bMaximized = !m_bMaximized;
					break;
				}
			}

			break;
		}

		case WM_QUIT:
			PostQuitMessage(0);
			break;

		case WM_DESTROY:
			PostQuitMessage(0);
			break;
		}
	}

	return;
}

HWND CMainWindow::GetHWND()
{
	return m_hWnd;
}

int CMainWindow::GetHeight()
{
	return m_iHeight;
}

int CMainWindow::GetWidth()
{
	return m_iWidth;
}

CMainWindow* CMainWindow::GetInstance()
{
	return m_pInstance;
}