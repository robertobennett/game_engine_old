#include <Include\Renderer_Dll\stdafx.h>
#include <Include\Renderer_Dll\Interfaces\ICamera.h>

ICamera::ICamera():
m_hMainWnd(NULL),
m_pSystemTimer(nullptr),
m_pSystemSettings(nullptr),
m_pMainConsole(nullptr)
{
	return;
}

ICamera::ICamera(HWND hWnd, CTimer* pTimer, CSettings * pSettings, CConsole * pConsole):
m_hMainWnd(hWnd),
m_pSystemTimer(pTimer),
m_pSystemSettings(pSettings),
m_pMainConsole(pConsole)
{
	return;
}

ICamera::~ICamera()
{
	return;
}

bool ICamera::Create()
{
	return true;
}

void ICamera::Update()
{
	return;
}

void ICamera::Shutdown()
{
	return;
}

XMFLOAT4X4 ICamera::GetViewMatrix() const
{
	return m_xmf4x4View;
}

XMFLOAT4X4 ICamera::GetProjectionMatrix() const
{
	return m_xmf4x4Proj;
}
