#include <Include\Renderer_Dll\stdafx.h>
#include <Include\Renderer_Dll\Interfaces\IShader.h>

IShader::IShader():
m_pD3D11InputLayout(nullptr),
m_pD3D11VertexShader(nullptr),
m_pD3D11PixelShader(nullptr),
m_pD3D11ConstantBuffer(nullptr)
{
	return;
}

IShader::~IShader()
{
	return;
}

bool IShader::Create()
{
	if (m_pD3D11InputLayout == nullptr || m_pD3D11VertexShader == nullptr || m_pD3D11PixelShader == nullptr || m_pD3D11ConstantBuffer == nullptr)
		return false;

	return true;
}

void IShader::Update()
{
	return;
}

void IShader::Draw()
{
	return;
}

void IShader::Destroy()
{
	if (m_pD3D11ConstantBuffer != nullptr)
	{
		m_pD3D11ConstantBuffer->Release();
		m_pD3D11ConstantBuffer = nullptr;
	}

	if (m_pD3D11PixelShader != nullptr)
	{
		m_pD3D11PixelShader->Release();
		m_pD3D11PixelShader = nullptr;
	}

	if (m_pD3D11VertexShader != nullptr)
	{
		m_pD3D11VertexShader->Release();
		m_pD3D11VertexShader = nullptr;
	}

	if (m_pD3D11InputLayout != nullptr)
	{
		m_pD3D11InputLayout->Release();
		m_pD3D11InputLayout = nullptr;
	}

	return;
}

void IShader::UpdateConstantBuffers(XMFLOAT4X4 xmf4x4World, XMFLOAT4X4 xmf4x4View, XMFLOAT4X4 xmf4x4Proj)
{
	return;
}

D3D11_PRIMITIVE_TOPOLOGY IShader::D3D11GetTopology() const
{
	return m_D3D11PrimitiveTopology;
}

ID3D11InputLayout * IShader::D3D11GetInputLayout() const
{
	return m_pD3D11InputLayout;
}

ID3D11VertexShader * IShader::D3D11GetVertexShader() const
{
	return m_pD3D11VertexShader;
}

ID3D11PixelShader * IShader::D3D11GetPixelShader() const
{
	return m_pD3D11PixelShader;
}

ID3D11Buffer * const * IShader::D3D11GetConstantBuffer() const
{
	return &m_pD3D11ConstantBuffer;
}