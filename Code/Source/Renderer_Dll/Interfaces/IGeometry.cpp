#include <Include\Renderer_Dll\stdafx.h>
#include <Include\Renderer_Dll\Interfaces\IGeometry.h>

IGeometry::IGeometry():
m_iVertexCount(0),
m_iIndicieCount(0),
m_iVertexBufferStride(0),
m_iVertexBufferOffset(0),
m_iIndexBufferOffset(0),
m_pD3D11VertexBuffer(nullptr),
m_pD3D11IndexBuffer(nullptr),
m_pD3D11RS(nullptr),
m_pBoundShader(nullptr)
{
	return;
}

IGeometry::~IGeometry()
{
	return;
}

bool IGeometry::Create()
{
	if (m_pD3D11VertexBuffer == nullptr || m_pBoundShader == nullptr || m_pD3D11RS == nullptr || m_iVertexCount <= 0)
		return false;

	return true;
}

void IGeometry::Update()
{
	return;
}

void IGeometry::Draw()
{
	return;
}

void IGeometry::Destroy()
{
	if (m_pBoundShader != nullptr)
	{
		m_pBoundShader = nullptr;
	}

	if (m_pD3D11RS != nullptr)
	{
		//Released in Direct3D
		m_pD3D11RS = nullptr;
	}

	if (m_pD3D11IndexBuffer != nullptr)
	{
		m_pD3D11IndexBuffer->Release();
		m_pD3D11IndexBuffer = nullptr;
	}

	if (m_pD3D11VertexBuffer != nullptr)
	{
		m_pD3D11VertexBuffer->Release();
		m_pD3D11VertexBuffer = nullptr;
	}

	return;
}

XMFLOAT4X4 IGeometry::GetWorldMatrix() const
{
	return m_xmf4x4WorldMatrix;
}

IShader * IGeometry::GetBoundShader() const
{
	return m_pBoundShader;
}

ID3D11Buffer * const * IGeometry::D3D11GetVertexBuffer() const
{
	return &m_pD3D11VertexBuffer;
}

UINT IGeometry::GetVertBuffStride() const
{
	return (m_iVertexBufferStride);
}

UINT IGeometry::GetVertBuffOffset() const
{
	return (m_iVertexBufferOffset);
}

ID3D11Buffer * IGeometry::D3D11GetIndexBuffer() const
{
	return m_pD3D11IndexBuffer;
}

UINT IGeometry::GetIndexBuffOffset() const
{
	return m_iIndexBufferOffset;
}

UINT IGeometry::GetVertexCount() const
{
	return m_iVertexCount;
}

UINT IGeometry::GetIndicieCount() const
{
	return m_iIndicieCount;
}

ID3D11RasterizerState * IGeometry::D3D11GetRS() const
{
	return m_pD3D11RS;
}

void IGeometry::CalculateFaceNormal(XMFLOAT3 *xmf3A, XMFLOAT3 *xmf3B, XMFLOAT3 *xmf3C, XMFLOAT3 *xmf3Out)
{
	XMVECTOR xmvAB = XMLoadFloat3(xmf3A) - XMLoadFloat3(xmf3B);
	XMVECTOR xmvAC = XMLoadFloat3(xmf3A) - XMLoadFloat3(xmf3C);
	XMVECTOR xmvNorm = XMVector3Cross(xmvAB, xmvAC);

	xmvNorm = XMVector3Normalize(xmvNorm);

	XMStoreFloat3(xmf3Out, xmvNorm);
}

void IGeometry::CalculateFaceNormal(XMFLOAT3 *xmf3A, XMFLOAT3 *xmf3B, XMFLOAT3 *xmf3C, XMVECTOR &xmvOut)
{
	XMVECTOR xmvAB = XMLoadFloat3(xmf3A) - XMLoadFloat3(xmf3B);
	XMVECTOR xmvAC = XMLoadFloat3(xmf3A) - XMLoadFloat3(xmf3C);
	XMVECTOR xmvNorm = XMVector3Cross(xmvAB, xmvAC);

	xmvNorm = XMVector3Normalize(xmvNorm);

	xmvOut = xmvNorm;
}