#include <Include\Renderer_Dll\stdafx.h>
#include <Include\Renderer_Dll\Renderer.h>

CRenderer::CRenderer():
m_bIsInitialized(false),
m_hWnd(NULL),
m_pSystemTimer(nullptr),
m_pSystemSettings(nullptr),
m_pMainConsole(nullptr),
m_pDirect3D(nullptr)
{
	return;
}

CRenderer::CRenderer(HWND hWnd, CTimer * pTimer, CSettings * pSettings, CConsole * pConsole):
m_bIsInitialized(false),
m_hWnd(hWnd),
m_pSystemTimer(pTimer),
m_pSystemSettings(pSettings),
m_pMainConsole(pConsole),
m_pDirect3D(nullptr)
{
	return;
}

CRenderer::~CRenderer()
{
	return;
}

bool CRenderer::Init(HWND hWnd)
{
	if (!m_bIsInitialized)
	{
		m_hWnd = hWnd;

		m_pMainConsole->WriteToConsole("Initializing Graphics System !");

		m_pDirect3D = new CDirect3D(m_hWnd, m_pSystemTimer, m_pSystemSettings, m_pMainConsole);
		if (!m_pDirect3D->Init(hWnd))
		{
			delete m_pDirect3D;
			MessageBoxA(NULL, "Failed to initialized Direct X", "Fatal Error", MB_OK);
			PostQuitMessage(0);

			return false;
		}

		m_bIsInitialized = true;
	}

	return true;
}

void CRenderer::Resize()
{
	m_pDirect3D->Resize();

	return;
}

void CRenderer::Update()
{
	m_pDirect3D->Update();

	return;
}

void CRenderer::Draw()
{
	m_pDirect3D->Draw();

	return;
}

void CRenderer::Shutdown()
{
	m_hWnd = NULL;

	m_pDirect3D->Shutdown();
	delete m_pDirect3D;
	m_pDirect3D = nullptr;

	return;
}

CDirect3D * const CRenderer::GetDirect3D() const
{
	return m_pDirect3D;
}
