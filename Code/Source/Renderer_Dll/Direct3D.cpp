#include <Include\Renderer_Dll\stdafx.h>
#include <Include\Renderer_Dll\Direct3D.h>

CDirect3D::CDirect3D() :
m_bInitialized(false),
m_bMSAAEnabled(true),
m_bRasterStateOverrode(false),
m_iMSAASampleCount(1),
m_iMSAASampleQuality(0),
m_iWidth(0),
m_iHeight(0),
m_hWnd(NULL),
m_pSystemTimer(nullptr),
m_pSystemSettings(nullptr),
m_pMainConsole(nullptr),
m_pD3D11Device(nullptr),
m_pD3D11DeviceContext(nullptr),
m_pD3D11Debug(nullptr),
m_pDXGISwapChain(nullptr),
m_pD3D11RenderTarget(nullptr),
m_pD3D11DepthStencilBuffer(nullptr),
m_pD3D11DepthStencilView(nullptr),
m_pD3D11RSDefault(nullptr),
m_pD3D11RSWireFrameCullBackface(nullptr),
m_pD3D11RSWireFrameNoCull(nullptr),
m_pBoundCamera(nullptr)
{
	return;
}

CDirect3D::CDirect3D(HWND hWnd, CTimer * pTimer, CSettings * pSettings, CConsole * pConsole) :
m_bInitialized(false),
m_bMSAAEnabled(true),
m_bRasterStateOverrode(false),
m_iMSAASampleCount(1),
m_iMSAASampleQuality(0),
m_iWidth(0),
m_iHeight(0),
m_hWnd(hWnd),
m_pSystemTimer(pTimer),
m_pSystemSettings(pSettings),
m_pMainConsole(pConsole),
m_pD3D11Device(nullptr),
m_pD3D11DeviceContext(nullptr),
m_pD3D11Debug(nullptr),
m_pDXGISwapChain(nullptr),
m_pD3D11RenderTarget(nullptr),
m_pD3D11DepthStencilBuffer(nullptr),
m_pD3D11DepthStencilView(nullptr),
m_pD3D11RSDefault(nullptr),
m_pD3D11RSWireFrameCullBackface(nullptr),
m_pD3D11RSWireFrameNoCull(nullptr),
m_pBoundCamera(nullptr)
{
	return;
}

CDirect3D::~CDirect3D()
{
	return;
}

bool CDirect3D::Init(HWND hWnd)
{
	m_hWnd = hWnd;

	m_pMainConsole->WriteToConsole("Initializing Direct X !");

	if (m_hWnd && !m_bInitialized)
	{
		//Create device & device context
		//Check MSAA
		//Retrive any settings
		//Get factory
		//Create swapchain
		//Create render target
		//Create depth stencil
		//Bind to pipeline
		//Set viewport

		HRESULT hRes;
		D3D_FEATURE_LEVEL featureLevel;
		m_pMainConsole->WriteToConsole("Creating D3D11 device !");
		hRes = D3D11CreateDevice(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, D3D11_CREATE_DEVICE_DEBUG, NULL, 0, D3D11_SDK_VERSION, &m_pD3D11Device, &featureLevel, &m_pD3D11DeviceContext);
		
		hRes = m_pD3D11Device->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&m_pD3D11Debug));

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! D3D11CreateDevice failed !");
			return false;
		}

		if (featureLevel != D3D_FEATURE_LEVEL_11_0)
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! DirectX 11 not supported !");
			return false;
		}

		if (m_pSystemSettings->GetVar("DirectX", "bFirstTime").b == true)
		{
			m_pMainConsole->WriteToConsole("Performing first time DirectX setup !");
			m_pMainConsole->WriteToConsole("Checking max supported MSAA !");
			UINT iSampleQuality = 0;
			for (UINT i = 4; i <= D3D11_MAX_MULTISAMPLE_SAMPLE_COUNT; i *= 2)
			{
				std::stringstream ssOut;
				ssOut << "Performing check: " << "MSAACount: " << i;
				m_pMainConsole->WriteToConsole(ssOut.str().c_str());
				m_pD3D11Device->CheckMultisampleQualityLevels(DXGI_FORMAT_R8G8B8A8_UNORM, i, &iSampleQuality);
				if (iSampleQuality == 0)
				{
					m_pMainConsole->WriteToConsole("Count to high, is not compatible !");
					ssOut.str("");
					ssOut.clear();
					ssOut << "Check finished ! MSAACount: " << m_iMSAASampleCount << " - MSAAQuality: " << m_iMSAASampleQuality;
					m_pMainConsole->WriteToConsole(ssOut.str().c_str());
					break;
				}

				m_iMSAASampleCount = i;
				m_iMSAASampleQuality = (iSampleQuality - 1);
			}

			m_pSystemSettings->SetVar("DirectX", "bFirstTime", false);
			m_pSystemSettings->SetVar("DirectX", "bMSAAEnabled", true);
			m_pSystemSettings->SetVar("DirectX", "iMSAASampleCount", m_iMSAASampleCount);
			m_pSystemSettings->SetVar("DirectX", "iMSAASampleQuality", m_iMSAASampleQuality);

			m_pSystemSettings->SaveSettings();
		}


		if (m_pSystemSettings->GetVar("DirectX", "bMSAAEnabled").b)
		{
			m_bMSAAEnabled = true;
			m_iMSAASampleCount = m_pSystemSettings->GetVar("DirectX", "iMSAASampleCount").i;
			m_iMSAASampleQuality = m_pSystemSettings->GetVar("DirectX", "iMSAASampleQuality").i;
		}
		else
		{
			m_bMSAAEnabled = false;
			m_iMSAASampleCount = 1;
			m_iMSAASampleQuality = 0;
		}

		m_pMainConsole->WriteToConsole("Retriving IDXGIDevice !");
		IDXGIDevice * pDXGIDevice = nullptr;
		hRes = m_pD3D11Device->QueryInterface(__uuidof(IDXGIDevice), (void**)&pDXGIDevice);

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't retrive IDXGIDevice !");
			return false;
		}

		m_pMainConsole->WriteToConsole("Retriving IDXGIAdapter !");
		IDXGIAdapter * pDXGIAdapter = nullptr;
		hRes = pDXGIDevice->GetAdapter(&pDXGIAdapter);

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't retrive IDXGIAdapter !");
			return false;
		}

		pDXGIDevice->Release();
		pDXGIDevice = nullptr;

		m_pMainConsole->WriteToConsole("Retriving IDXGIFactory !");
		IDXGIFactory * pDXGIFactory = nullptr;
		hRes = pDXGIAdapter->GetParent(__uuidof(IDXGIFactory), (void**)&pDXGIFactory);

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't retrive IDXGIFactory !");
			return false;
		}

		pDXGIAdapter->Release();
		pDXGIAdapter = nullptr;

		RECT rc; ZeroMemory(&rc, sizeof(RECT));
		GetClientRect(m_hWnd, &rc);
		m_iHeight = rc.bottom - rc.top;
		m_iWidth = rc.right - rc.left;

		m_pMainConsole->WriteToConsole("Creating the swapchain !");
		DXGI_SWAP_CHAIN_DESC sd; ZeroMemory(&sd, sizeof(DXGI_SWAP_CHAIN_DESC));
		sd.BufferCount = 1;
		sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
		sd.BufferDesc.Height = m_iHeight;
		sd.BufferDesc.Width = m_iWidth;
		sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
		sd.BufferDesc.RefreshRate.Numerator = 0;
		sd.BufferDesc.RefreshRate.Denominator = 1;
		sd.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
		sd.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
		sd.SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
		sd.SampleDesc.Count = m_iMSAASampleCount;
		sd.SampleDesc.Quality = m_iMSAASampleQuality;
		sd.Windowed = true;
		sd.Flags = 0;
		sd.OutputWindow = m_hWnd;

		hRes = pDXGIFactory->CreateSwapChain(m_pD3D11Device, &sd, &m_pDXGISwapChain);

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't create a swapchain !");
			return false;
		}

		pDXGIFactory->Release();
		pDXGIFactory = nullptr;

		m_pMainConsole->WriteToConsole("Creating the rendertarget view !");
		m_pMainConsole->WriteToConsole("Getting back buffer !");
		ID3D11Texture2D * pBackBuffer;
		hRes = m_pDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer));

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't get back buffer !");
			return false;
		}

		hRes = m_pD3D11Device->CreateRenderTargetView(pBackBuffer, NULL, &m_pD3D11RenderTarget);

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't create render target view !");
			return false;
		}

		pBackBuffer->Release();
		pBackBuffer = nullptr;

		m_pMainConsole->WriteToConsole("Creating the depth stencil !");

		D3D11_TEXTURE2D_DESC ds; ZeroMemory(&ds, sizeof(D3D11_TEXTURE2D_DESC));
		ds.Width = m_iWidth;
		ds.Height = m_iHeight;
		ds.MipLevels = 1;
		ds.ArraySize = 1;
		ds.SampleDesc.Count = m_iMSAASampleCount;
		ds.SampleDesc.Quality = m_iMSAASampleQuality;
		ds.Usage = D3D11_USAGE_DEFAULT;
		ds.BindFlags = D3D11_BIND_DEPTH_STENCIL;
		ds.CPUAccessFlags = 0;
		ds.MiscFlags = 0;
		ds.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

		hRes = m_pD3D11Device->CreateTexture2D(&ds, NULL, &m_pD3D11DepthStencilBuffer);

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't create depth stencil buffer !");
			return false;
		}

		hRes = m_pD3D11Device->CreateDepthStencilView(m_pD3D11DepthStencilBuffer, NULL, &m_pD3D11DepthStencilView);

		if (FAILED(hRes))
		{
			m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't create depth stencil view !");
			return false;
		}

		m_pMainConsole->WriteToConsole("Setting the render target !");
		m_pD3D11DeviceContext->OMSetRenderTargets(1, &m_pD3D11RenderTarget, m_pD3D11DepthStencilView);

		m_pMainConsole->WriteToConsole("Creating the view port !");
		D3D11_VIEWPORT vp;
		vp.Height = static_cast<float>(m_iHeight);
		vp.Width = static_cast<float>(m_iWidth);
		vp.MaxDepth = 1.0f;
		vp.MinDepth = 0.0f;
		vp.TopLeftX = 0;
		vp.TopLeftY = 0;

		m_pMainConsole->WriteToConsole("Setting the view port !");
		m_pD3D11DeviceContext->RSSetViewports(1, &vp);
		
		//Create default RS
		D3D11_RASTERIZER_DESC rd; ZeroMemory(&rd, sizeof(D3D11_RASTERIZER_DESC));
		rd.CullMode = D3D11_CULL_BACK;
		rd.FillMode = D3D11_FILL_SOLID;

		hRes = m_pD3D11Device->CreateRasterizerState(&rd, &m_pD3D11RSDefault);
		if (FAILED(hRes))
			return false;

		//Create default RS no cull
		ZeroMemory(&rd, sizeof(D3D11_RASTERIZER_DESC));
		rd.CullMode = D3D11_CULL_NONE;
		rd.FillMode = D3D11_FILL_SOLID;

		hRes = m_pD3D11Device->CreateRasterizerState(&rd, &m_pD3D11RSDefaultNoCull);
		if (FAILED(hRes))
			return false;

		//Create wireframe cull RS
		ZeroMemory(&rd, sizeof(D3D11_RASTERIZER_DESC));
		rd.CullMode = D3D11_CULL_BACK;
		rd.FillMode = D3D11_FILL_WIREFRAME;

		hRes = m_pD3D11Device->CreateRasterizerState(&rd, &m_pD3D11RSWireFrameCullBackface);
		if (FAILED(hRes))
			return false;

		//Create wireframe no cull RS
		ZeroMemory(&rd, sizeof(D3D11_RASTERIZER_DESC));
		rd.CullMode = D3D11_CULL_NONE;
		rd.FillMode = D3D11_FILL_WIREFRAME;
		
		hRes = m_pD3D11Device->CreateRasterizerState(&rd, &m_pD3D11RSWireFrameNoCull);
		if (FAILED(hRes))
			return false;

		m_pD3D11DeviceContext->RSSetState(m_pD3D11RSDefault);
		return true;
	}

	m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! DirectX is either already initialized or the window handle passed is invalid !");
	return false;
}

void CDirect3D::Resize()
{
	m_pMainConsole->WriteToConsole("CDirect3D::Resize() called !");

	HRESULT hRes = S_OK;

	m_pMainConsole->WriteToConsole("Releasing render target !");
	m_pD3D11RenderTarget->Release();
	m_pD3D11RenderTarget = nullptr;

	m_pMainConsole->WriteToConsole("Releasing depth stencil buffer !");
	m_pD3D11DepthStencilBuffer->Release();
	m_pD3D11DepthStencilBuffer = nullptr;

	m_pMainConsole->WriteToConsole("Releasing depth stencil view !");
	m_pD3D11DepthStencilView->Release();
	m_pD3D11DepthStencilView = nullptr;

	m_pMainConsole->WriteToConsole("Resizing swapchain buffers !");
	m_pDXGISwapChain->ResizeBuffers(1, m_iWidth, m_iHeight, DXGI_FORMAT_R8G8B8A8_UNORM, 0);

	m_pMainConsole->WriteToConsole("Creating the rendertarget view !");
	m_pMainConsole->WriteToConsole("Getting back buffer !");
	ID3D11Texture2D * pBackBuffer;
	hRes = m_pDXGISwapChain->GetBuffer(0, __uuidof(ID3D11Texture2D), reinterpret_cast<void**>(&pBackBuffer));

	if (FAILED(hRes))
	{
		m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't get back buffer !");
		return;
	}

	hRes = m_pD3D11Device->CreateRenderTargetView(pBackBuffer, NULL, &m_pD3D11RenderTarget);

	if (FAILED(hRes))
	{
		m_pMainConsole->WriteToConsole("Failed to initialize DirectX ! Couldn't create render target view !");
		return;
	}

	pBackBuffer->Release();
	pBackBuffer = nullptr;

	m_pMainConsole->WriteToConsole("Recreating depth stencil !");
	D3D11_TEXTURE2D_DESC ds; ZeroMemory(&ds, sizeof(D3D11_TEXTURE2D_DESC));
	ds.Width = m_iWidth;
	ds.Height = m_iHeight;
	ds.MipLevels = 1;
	ds.ArraySize = 1;
	ds.SampleDesc.Count = m_iMSAASampleCount;
	ds.SampleDesc.Quality = m_iMSAASampleQuality;
	ds.Usage = D3D11_USAGE_DEFAULT;
	ds.BindFlags = D3D11_BIND_DEPTH_STENCIL;
	ds.CPUAccessFlags = 0;
	ds.MiscFlags = 0;
	ds.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

	hRes = m_pD3D11Device->CreateTexture2D(&ds, NULL, &m_pD3D11DepthStencilBuffer);

	if (FAILED(hRes))
	{
		m_pMainConsole->WriteToConsole("Failed to resize ! Couldn't create depth stencil buffer !");
		return;
	}

	hRes = m_pD3D11Device->CreateDepthStencilView(m_pD3D11DepthStencilBuffer, NULL, &m_pD3D11DepthStencilView);

	if (FAILED(hRes))
	{
		m_pMainConsole->WriteToConsole("Failed to resize ! Couldn't create depth stencil view !");
		return;
	}

	m_pMainConsole->WriteToConsole("Creating the view port !");
	D3D11_VIEWPORT vp;
	vp.Height = static_cast<float>(m_iHeight);
	vp.Width = static_cast<float>(m_iWidth);
	vp.MaxDepth = 1.0f;
	vp.MinDepth = 0.0f;
	vp.TopLeftX = 0;
	vp.TopLeftY = 0;

	m_pMainConsole->WriteToConsole("Setting the view port !");
	m_pD3D11DeviceContext->RSSetViewports(1, &vp);

	return;
}

void CDirect3D::Update()
{
	float fColour[4] = { 0.0f, 0.0, 255.0f, 255.0f };
	m_pD3D11DeviceContext->ClearRenderTargetView(m_pD3D11RenderTarget, fColour);
	m_pD3D11DeviceContext->ClearDepthStencilView(m_pD3D11DepthStencilView, D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0);

	if (m_pBoundCamera == nullptr)
		return;

	if(m_pRegisteredGeometry.size() > 0)
	{
		static IShader * pPrevShader = nullptr;
		for (UINT i = 0; i < m_pRegisteredGeometry.size(); i++)
		{
			if (m_pRegisteredGeometry[i]->GetBoundShader() == nullptr)
				break;

			//If the previous shader is the same there is no point in updating the shaders, topology or input layout as they will be the same
			if (m_pRegisteredGeometry[i]->GetBoundShader() != pPrevShader)
			{
				m_pD3D11DeviceContext->IASetPrimitiveTopology(m_pRegisteredGeometry[i]->GetBoundShader()->D3D11GetTopology());
				m_pD3D11DeviceContext->IASetInputLayout(m_pRegisteredGeometry[i]->GetBoundShader()->D3D11GetInputLayout());
				m_pD3D11DeviceContext->VSSetShader(m_pRegisteredGeometry[i]->GetBoundShader()->D3D11GetVertexShader(), NULL, 0);
				m_pD3D11DeviceContext->PSSetShader(m_pRegisteredGeometry[i]->GetBoundShader()->D3D11GetPixelShader(), NULL, 0);
			}

			m_pRegisteredGeometry[i]->GetBoundShader()->UpdateConstantBuffers(m_pRegisteredGeometry[i]->GetWorldMatrix(), m_pBoundCamera->GetViewMatrix(), m_pBoundCamera->GetProjectionMatrix());
			m_pD3D11DeviceContext->VSSetConstantBuffers(0, 1, m_pRegisteredGeometry[i]->GetBoundShader()->D3D11GetConstantBuffer());

			UINT iStride = m_pRegisteredGeometry[i]->GetVertBuffStride();
			UINT iOffset = m_pRegisteredGeometry[i]->GetVertBuffOffset();
			m_pD3D11DeviceContext->IASetVertexBuffers(0, 1, m_pRegisteredGeometry[i]->D3D11GetVertexBuffer(), &iStride, &iOffset);

			if (m_pRegisteredGeometry[i]->D3D11GetIndexBuffer() == nullptr)
			{
				m_pD3D11DeviceContext->Draw(m_pRegisteredGeometry[i]->GetVertexCount(), 0);
			}
			else
			{
				m_pD3D11DeviceContext->IASetIndexBuffer(m_pRegisteredGeometry[i]->D3D11GetIndexBuffer(), DXGI_FORMAT_R32_UINT, m_pRegisteredGeometry[i]->GetIndexBuffOffset());
				
				if(!m_bRasterStateOverrode)
					m_pD3D11DeviceContext->RSSetState(m_pRegisteredGeometry[i]->D3D11GetRS());

				m_pD3D11DeviceContext->DrawIndexed(m_pRegisteredGeometry[i]->GetIndicieCount(), 0, 0);
			}

			pPrevShader = m_pRegisteredGeometry[i]->GetBoundShader();
		}
	}
	return;
}

void CDirect3D::Draw()
{
	m_pDXGISwapChain->Present(0, 0);

	return;
}

void CDirect3D::Shutdown()
{
	m_pSystemSettings->SetVar("DirectX", "bMSAAEnabled", m_bMSAAEnabled);
	m_pSystemSettings->SetVar("DirectX", "iMSAASampleCount", m_iMSAASampleCount);
	m_pSystemSettings->SetVar("DirectX", "iMSAASampleQuality", m_iMSAASampleQuality);

	m_pSystemSettings->SaveSettings();

	if (m_pD3D11RSWireFrameNoCull != nullptr)
	{
		m_pD3D11RSWireFrameNoCull->Release();
		m_pD3D11RSWireFrameNoCull = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11RSWireFrameNoCull released !");
	}

	if (m_pD3D11RSWireFrameCullBackface != nullptr)
	{
		m_pD3D11RSWireFrameCullBackface->Release();
		m_pD3D11RSWireFrameCullBackface = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11RSWireFrameCullBackface released !");
	}

	if (m_pD3D11RSDefaultNoCull != nullptr)
	{
		m_pD3D11RSDefaultNoCull->Release();
		m_pD3D11RSDefaultNoCull = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11RSDefaultNoCull released !");
	}

	if (m_pD3D11RSDefault != nullptr)
	{
		m_pD3D11RSDefault->Release();
		m_pD3D11RSDefault = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11RSDefault released !");
	}

	if (m_pD3D11DepthStencilView != nullptr)
	{
		m_pD3D11DepthStencilView->Release();
		m_pD3D11DepthStencilView = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11DepthStencilView released !");
	}

	if (m_pD3D11DepthStencilBuffer != nullptr)
	{
		m_pD3D11DepthStencilBuffer->Release();
		m_pD3D11DepthStencilBuffer = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11DepthStencilBuffer released !");
	}

	if (m_pD3D11RenderTarget != nullptr)
	{
		m_pD3D11RenderTarget->Release();
		m_pD3D11RenderTarget = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11RenderTarget released !");
	}

	if (m_pDXGISwapChain != nullptr)
	{
		m_pDXGISwapChain->Release();
		m_pDXGISwapChain = nullptr;
		m_pMainConsole->WriteToConsole("m_pDXGISwapChain released !");
	}

	if (m_pD3D11DeviceContext != nullptr)
	{
		m_pD3D11DeviceContext->Release();
		m_pD3D11DeviceContext = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11DeviceContext released !");
	}

	if (m_pD3D11Device != nullptr)
	{
		m_pD3D11Device->Release();
		m_pD3D11Device = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11Device released !");
	}

	OutputDebugStringA("//////REPORTING LIVE OBJECTS !");
	m_pD3D11Debug->ReportLiveDeviceObjects(D3D11_RLDO_DETAIL);
	OutputDebugStringA("//////FINISED REPORTING LIVE OBJECTS !");

	if (m_pD3D11Debug != nullptr)
	{
		m_pD3D11Debug->Release();
		m_pD3D11Debug = nullptr;
		m_pMainConsole->WriteToConsole("m_pD3D11Debug released !");
	}

	return;
}

void CDirect3D::ResetRasterState()
{
	m_bRasterStateOverrode = false;

	return;
}

bool CDirect3D::SetRasterState(ID3D11RasterizerState * pRS)
{
	if (pRS == nullptr)
		return false;

	m_pD3D11DeviceContext->RSSetState(pRS);
	m_bRasterStateOverrode = true;

	return true;
}

bool CDirect3D::BindCamera(ICamera * pCamera)
{
	if(pCamera == nullptr)
		return false;

	m_pBoundCamera = pCamera;

	return true;
}

bool CDirect3D::RegisterGeometry(IGeometry * pGeom)
{
	if (m_pRegisteredGeometry.size() > 0)
	{
		for (UINT i = 0; i < m_pRegisteredGeometry.size(); i++)
		{
			if (m_pRegisteredGeometry[i] == pGeom)
				return false;
		}
	}

	m_pRegisteredGeometry.push_back(pGeom);

	return true;
}

bool CDirect3D::UnregisterGeometry(IGeometry * pGeom)
{
	if (m_pRegisteredGeometry.size() > 0)
	{
		for (UINT i = 0; i < m_pRegisteredGeometry.size(); i++)
		{
			if (m_pRegisteredGeometry[i] == pGeom)
			{
				m_pRegisteredGeometry.erase(m_pRegisteredGeometry.begin() + i);
				return true;
			}
		}
	}

	return false;
}

HRESULT CDirect3D::D3D11CreateInputLayout(D3D11_INPUT_ELEMENT_DESC * pDesc, UINT iElementCount, const void * pShaderBytecode, SIZE_T iByteCodeSize, ID3D11InputLayout ** pLayout)
{
	if (m_pD3D11Device == nullptr)
		return STG_E_INVALIDPOINTER;

	return m_pD3D11Device->CreateInputLayout(pDesc, iElementCount, pShaderBytecode, iByteCodeSize, pLayout);
}

HRESULT CDirect3D::D3D11CreatePixelShader(const void * pShaderBytecode, SIZE_T iBytecodeSize, ID3D11ClassLinkage * pClsLnk, ID3D11PixelShader ** pPixelShader)
{
	if (m_pD3D11Device == nullptr)
		return STG_E_INVALIDPOINTER;

	return m_pD3D11Device->CreatePixelShader(pShaderBytecode, iBytecodeSize, pClsLnk, pPixelShader);
}

HRESULT CDirect3D::D3D11CreateVertexShader(const void * pShaderBytecode, SIZE_T iBytecodeSize, ID3D11ClassLinkage * pClsLnk, ID3D11VertexShader ** pVertexShader)
{
	if(m_pD3D11Device == nullptr)
		return STG_E_INVALIDPOINTER;

	return m_pD3D11Device->CreateVertexShader(pShaderBytecode, iBytecodeSize, pClsLnk, pVertexShader);
}

HRESULT CDirect3D::D3D11CreateBuffer(D3D11_BUFFER_DESC * pBuffDesc, D3D11_SUBRESOURCE_DATA * pInitData, ID3D11Buffer ** pBuffer)
{
	if (m_pD3D11Device == nullptr)
		return STG_E_INVALIDPOINTER;

	return m_pD3D11Device->CreateBuffer(pBuffDesc, pInitData, pBuffer);
}

HRESULT CDirect3D::D3D11Map(ID3D11Resource * pResource, UINT iSubresource, D3D11_MAP mapType, UINT iMapFlags, D3D11_MAPPED_SUBRESOURCE * pMappedResource)
{
	if (m_pD3D11DeviceContext == nullptr)
		return STG_E_INVALIDPOINTER;

	return m_pD3D11DeviceContext->Map(pResource, iSubresource, mapType, iMapFlags, pMappedResource);
}

bool CDirect3D::D3D11Unmap(ID3D11Resource * pResource, UINT iSubresource)
{
	if (m_pD3D11DeviceContext == nullptr)
		return false;

	m_pD3D11DeviceContext->Unmap(pResource, iSubresource);

	return true;
}

ID3D11RasterizerState * CDirect3D::GetRSDefault() const
{
	return m_pD3D11RSDefault;
}

ID3D11RasterizerState * CDirect3D::GetRSDefaultNoCull() const
{
	return m_pD3D11RSDefaultNoCull;
}

ID3D11RasterizerState * CDirect3D::GetRSWireFrameCullBack() const
{
	return m_pD3D11RSWireFrameCullBackface;
}

ID3D11RasterizerState * CDirect3D::GetRSWireFrameNoCull() const
{
	return m_pD3D11RSWireFrameNoCull;
}
