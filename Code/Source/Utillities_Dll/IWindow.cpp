#include <Include\Utillities_Dll\stdafx.h>
#include <Include\Utillities_Dll\IWindow.h>

WNDPROC IWindow::WindowProcedure = CEventHandler::WndProc;
CEventHandler* IWindow::m_pEventHandler = CEventHandler::GetInstance();

bool IWindow::Create()
{
	return true;
}

bool IWindow::Create(HWND hWndParent)
{
	return true;
}

void IWindow::Resize()
{
	return;
}

void IWindow::Update()
{
	return;
}

void IWindow::Draw()
{
	return;
}

HWND IWindow::GetHWND()
{
	return m_hWnd;
}

int IWindow::GetHeight()
{
	return m_iHeight;
}

int IWindow::GetWidth()
{
	return m_iWidth;
}

void IWindow::Destroy()
{
	return;
}

bool IWindow::RegisterChildWindow(IWindow* pWnd)
{
	if (m_ChildWindows.size() > 0)
	{
		for (UINT i = 0; i < m_ChildWindows.size(); i++)
		{
			if (m_ChildWindows[i] == pWnd)
			{
				return false;
			}
		}

		m_ChildWindows.push_back(pWnd);
		return true;
	}
	else
	{
		m_ChildWindows.push_back(pWnd);
		return true;
	}

	return false;
}

bool IWindow::UnregisterChildWindow(IWindow* pWnd)
{
	if (m_ChildWindows.size() > 0)
	{
		for (UINT i = 0; i < m_ChildWindows.size(); i++)
		{
			if (m_ChildWindows[i] == pWnd)
			{
				pWnd->Destroy();
				DestroyWindow(pWnd->GetHWND());
				m_ChildWindows.erase((m_ChildWindows.begin() + i));
				return true;
			}
		}
	}

	return false;
}

DLL_EXPORT_IMPORT WNDPROC IWindow::GetWndProcHandle()
{
	return WindowProcedure;
}