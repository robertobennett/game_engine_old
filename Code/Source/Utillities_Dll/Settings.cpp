#include <Include\Utillities_Dll\stdafx.h>
#include <Include\Utillities_Dll\Settings.h>

std::string CSettings::m_s_sFilePath = ""; //Initialize as blank as in constructor we will set the file path
CSettings* CSettings::m_s_pInstance = CSettings::GetInstance();

CSettings::CSettings()
{
	std::stringstream ssFilePath;
	ssFilePath << GetRootFolderPath();
	ssFilePath << "Settings\\config.settings\0";

	OutputDebugStringA(ssFilePath.str().c_str());

	m_s_sFilePath = ssFilePath.str();

	return;
}

CSettings::~CSettings()
{
	m_pFileManager->Destroy();
	delete m_pFileManager;
	m_pFileManager = 0;

	return;
}

CSettings* CSettings::GetInstance()
{
	if (!m_s_pInstance)
	{
		m_s_pInstance = new CSettings();

		m_s_pInstance->Init();
	}
	return m_s_pInstance;
}

void CSettings::Init()
{
	m_pFileManager = new CFileManager();
	m_pFileManager->Create(m_s_sFilePath);
	m_pFileManager->ReadFile();
	m_pFileManager->DumpToDebug();
	m_pData = m_pFileManager->GetData();
	
	return;
}

void CSettings::SaveSettings()
{
	m_pFileManager->UpdateData(m_pData);
	m_pFileManager->DumpToDebug();

	return;
}

void CSettings::SetVar(std::string sClassificationName, std::string sVarName, bool bVarValue)
{
	if (m_pData.size() > 0)
	{
		for (UINT i = 0; i < m_pData.size(); i++)
		{
			if (m_pData[i]->sClassification == sClassificationName && m_pData[i]->sName == sVarName)
			{
				if (bVarValue == true)
				{
					m_pData[i]->sValue = "1";
					return;
				}
				else
				{
					m_pData[i]->sValue = "0";
				}
			}
		}
	}

	return;
}

void CSettings::SetVar(std::string sClassificationName, std::string sVarName, int iVarValue)
{
	if (m_pData.size() > 0)
	{
		for (UINT i = 0; i < m_pData.size(); i++)
		{
			if (m_pData[i]->sClassification == sClassificationName && m_pData[i]->sName == sVarName)
			{
				m_pData[i]->sValue = std::to_string(iVarValue);;
			}
		}
	}

	return;
}

void CSettings::SetVar(std::string sClassificationName, std::string sVarName, unsigned int iVarValue)
{
	if (m_pData.size() > 0)
	{
		for (UINT i = 0; i < m_pData.size(); i++)
		{
			if (m_pData[i]->sClassification == sClassificationName && m_pData[i]->sName == sVarName)
			{
				m_pData[i]->sValue = std::to_string(iVarValue);;
			}
		}
	}

	return;
}

void CSettings::SetVar(std::string sClassificationName, std::string sVarName, double dVarValue)
{
	if (m_pData.size() > 0)
	{
		for (UINT i = 0; i < m_pData.size(); i++)
		{
			if (m_pData[i]->sClassification == sClassificationName && m_pData[i]->sName == sVarName)
			{
				m_pData[i]->sValue = std::to_string(dVarValue);;
			}
		}
	}

	return;
}

VarReturnType CSettings::GetVar(std::string sClassificationName, std::string sVarName)
{
	VarReturnType returnValue; ZeroMemory(&returnValue, sizeof(VarReturnType));

	if (m_pData.size() > 0)
	{
		for (UINT i = 0; i < m_pData.size(); i++)
		{
			if (m_pData[i]->sClassification == sClassificationName && m_pData[i]->sName == sVarName)
			{
				if (m_pData[i]->sName[0] == 'b')
				{
					if (atoi(m_pData[i]->sValue.c_str()) == 0)
						returnValue.b = false;
					else
						returnValue.b = true;

					return returnValue;
				}
				else if (m_pData[i]->sName[0] == 'i')
				{
					returnValue.i = static_cast<unsigned int>(atoll(m_pData[i]->sValue.c_str()));
					return returnValue;
				}
				else if (m_pData[i]->sName[0] == 'd')
				{
					returnValue.d = atof(m_pData[i]->sValue.c_str());
					return returnValue;
				}
			}
		}
	}

	return returnValue;
}