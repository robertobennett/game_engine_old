#include <Include\Utillities_Dll\stdafx.h>

const std::string GetRootFolderPath()
{
	//As the file path to everyones Game_Engine folder will be different we need a way to retrive the file path
	char cBuff[LONGEST_FILE_PATH];
	//Get the file name including the full file path https://msdn.microsoft.com/en-us/library/windows/desktop/ms683197(v=vs.85).aspx
	GetModuleFileNameA(NULL, cBuff, sizeof(cBuff));
	//Create a string stream as it is easy to add the characters one by one and make changes again this could do with changing not the cleanest code
	std::stringstream ssFilePath;
	//Starting from the end of the path go back until the first '\' remember it is an escape sequence so look for '\\'
	for (int i = sizeof(cBuff); i >= 0; i--)
	{
		if (cBuff[i] == '\\')
		{
			//Then enter another look adding all the characters from there until the last \ this removes the file name (engine.exe)
			for (int x = 0; x < i; x++)
				ssFilePath << cBuff[x];

			//Break from loop as weve removed the file name which is the aim of this bit of code
			break;
		}

	}

	//Finish the file path
	ssFilePath << "\\..\\\0";

	return ssFilePath.str();
}

const std::wstring wGetRootFolderPath()
{
	//As the file path to everyones Game_Engine folder will be different we need a way to retrive the file path
	char cBuff[LONGEST_FILE_PATH];
	//Get the file name including the full file path https://msdn.microsoft.com/en-us/library/windows/desktop/ms683197(v=vs.85).aspx
	GetModuleFileNameA(NULL, cBuff, sizeof(cBuff));
	//Create a string stream as it is easy to add the characters one by one and make changes again this could do with changing not the cleanest code
	std::wstringstream wssFilePath;
	//Starting from the end of the path go back until the first '\' remember it is an escape sequence so look for '\\'
	for (int i = sizeof(cBuff); i >= 0; i--)
	{
		if (cBuff[i] == '\\')
		{
			//Then enter another look adding all the characters from there until the last \ this removes the file name (engine.exe)
			for (int x = 0; x < i; x++)
				wssFilePath << cBuff[x];

			//Break from loop as weve removed the file name which is the aim of this bit of code
			break;
		}

	}

	//Finish the file path
	wssFilePath << "\\..\\\0";

	return wssFilePath.str();
}
