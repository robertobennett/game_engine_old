#include <Include\Utillities_Dll\stdafx.h>
#include <Include\Utillities_Dll\Timer.h>

CTimer::CTimer() :
m_bStarted(false),
m_bPaused(false),
m_iTicksPerSec(0),
m_iStartPauseTicks(0),
m_iAcumelatedPausedTicks(0),
m_iStartTicks(0),
m_iPreviousTicks(0),
m_iCurrentTicks(0)
{
	return;
}

CTimer::~CTimer()
{
	return;
}

void CTimer::Start()
{
	if (!m_bStarted)
	{
		QueryPerformanceFrequency(reinterpret_cast<LARGE_INTEGER*> (&m_iTicksPerSec));
		QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&m_iStartTicks));

		m_iCurrentTicks = m_iStartTicks;

		m_bPaused = false;
		m_bStarted = true;

		return;
	}

	return;
}

void CTimer::Tick()
{
	if (m_bStarted)
	{
		m_iPreviousTicks = m_iCurrentTicks;
		QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&m_iCurrentTicks));

		return;
	}

	return;
}

void CTimer::Pause()
{
	if (m_bStarted)
	{
		if (m_bPaused)
			return;
		else
		{
			QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&m_iStartPauseTicks));

			m_bPaused = true;
			return;
		}

		return;
	}

	return;
}

void CTimer::Unpause()
{
	if (m_bStarted)
	{
		if (m_bPaused)
		{
			__int64 iEndPauseTicks = 0;
			QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&iEndPauseTicks));

			m_iAcumelatedPausedTicks += (iEndPauseTicks - m_iStartPauseTicks);
			m_iStartPauseTicks = 0;

			m_bPaused = false;
			return;
		}
		else
			return;

		return;
	}

	return;
}

void CTimer::TogglePause()
{
	if (m_bStarted)
	{
		if (m_bPaused)
		{
			__int64 iEndPauseTicks = 0;
			QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&iEndPauseTicks));

			m_iAcumelatedPausedTicks += (iEndPauseTicks - m_iStartPauseTicks);
			m_iStartPauseTicks = 0;

			m_bPaused = false;
			return;
		}
		else
		{
			QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&m_iStartPauseTicks));

			m_bPaused = true;
			return;
		}

		return;
	}

	return;
}

void CTimer::Reset()
{
	if (m_bStarted)
	{
		QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&m_iStartTicks));
		m_iStartPauseTicks = 0;
		m_iAcumelatedPausedTicks = 0;

		return;
	}

	return;
}

void CTimer::Stop()
{
	if (m_bStarted)
	{
		m_bStarted = false;
	}

	return;
}

int CTimer::GetTimeElapsed()
{
	if (m_bStarted)
	{
		__int64 iCurrentTicks = 0;
		QueryPerformanceCounter(reinterpret_cast<LARGE_INTEGER*> (&iCurrentTicks));

		if (m_bPaused)
			return static_cast<int> ((m_iStartPauseTicks - (m_iStartTicks + m_iAcumelatedPausedTicks)) / m_iTicksPerSec);
		else
			return static_cast<int> ((iCurrentTicks - (m_iStartTicks + m_iAcumelatedPausedTicks)) / m_iTicksPerSec);
	}

	return -1;
}

double CTimer::GetDeltaT()
{
	if (m_bStarted)
	{
		return static_cast<double> (static_cast<double> (m_iCurrentTicks - m_iPreviousTicks) / static_cast<double> (m_iTicksPerSec));
	}

	return -1;
}