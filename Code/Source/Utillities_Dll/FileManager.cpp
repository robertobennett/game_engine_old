#include <Include\Utillities_Dll\stdafx.h>
#include <Include\Utillities_Dll\FileManager.h>

CFileManager::CFileManager() :
m_iDumpNumber(0)
{
	return;
}

CFileManager::~CFileManager()
{
	return;
}

void CFileManager::DumpToDebug()
{
	m_iDumpNumber++;
	OutputDebugStringA("////////////////////\n");
	OutputDebugStringA("FILEMANAGER DUMP #");
	std::stringstream sss;
	sss << m_iDumpNumber;
	OutputDebugStringA(sss.str().c_str());
	OutputDebugStringA(" FOR: ");
	OutputDebugStringA(m_sFilePath.c_str());
	OutputDebugStringA("\n");
	OutputDebugStringA("Lines: \n");
	for (UINT i = 0; i < m_pLines.size(); i++)
	{
		std::string sLine;
		OutputDebugStringA(m_pLines[i]);
		OutputDebugStringA("\n");
	}
	OutputDebugStringA("Classifications: \n");
	for (UINT i = 0; i < m_Classifications.size(); i++)
	{
		std::stringstream ss;
		OutputDebugStringA(m_Classifications[i]->sName.c_str());
		OutputDebugStringA("\nLine Bounds: \n");
		ss << m_Classifications[i]->iLineBounds[0] << " - " << m_Classifications[i]->iLineBounds[1];
		OutputDebugStringA(ss.str().c_str());
		OutputDebugStringA("\nNumber Of Peices Of Data: \n");
		ss.str("");
		ss.clear();
		ss << m_Classifications[i]->iNumOfData;
		OutputDebugStringA(ss.str().c_str());
		OutputDebugStringA("\n");
	}
	OutputDebugStringA("Data: \n");
	for (UINT i = 0; i < m_Data.size(); i++)
	{
		OutputDebugStringA(m_Data[i]->sClassification.c_str());
		OutputDebugStringA("::");
		OutputDebugStringA(m_Data[i]->sName.c_str());
		OutputDebugStringA(" = ");
		OutputDebugStringA(m_Data[i]->sValue.c_str());
		OutputDebugStringA("\n");
	}
	OutputDebugStringA("////////////////////\n");
	return;
}

bool CFileManager::Create(std::string sFilePath)
{
	m_sFilePath = sFilePath;
	m_fsFile.open(m_sFilePath, std::fstream::in);

	if (m_fsFile.is_open())
	{
		return true;
	}

	return false;
}

bool CFileManager::ReadFile()
{
	//Clear old data
	if (m_pLines.size() > 0)
	{
		for (UINT i = 0; i < m_pLines.size(); i++)
		{
			SafeDelete(m_pLines[i]);
		}

		m_pLines.clear();
	}

	if (m_Classifications.size() > 0)
	{
		for (UINT i = 0; i < m_Classifications.size(); i++)
		{
			SafeDelete(m_Classifications[i]);
		}

		m_Classifications.clear();
	}

	if (m_Data.size() > 0)
	{
		for (UINT i = 0; i < m_Data.size(); i++)
		{
			SafeDelete(m_Data[i]);
		}

		m_Data.clear();
	}

	//Read all the stuff from the file
	if (!ReadLines())
		return false;

	if (!ReadClassifications())
		return false;

	if (!ReadData())
		return false;

	return true;
}

bool CFileManager::ReadLines()
{
	//Read the lines from the file into buffer
	//Split up lines from buffer into seperate lines
	//Parase through the lines to find the classifications, storing their boundries and name
	//Parase through each classification set to read the data, storing name, classification belonging to, value as strings
	//Store data, classifications ect in vectors passed
	//Clean up buffer, lines, classifications, data

	//Check if file is open, if it is close it...
	if (m_fsFile.is_open())
		m_fsFile.close();

	//...Open it in read mode
	m_fsFile.open(m_sFilePath, std::fstream::in);

	//Check file opened
	if (m_fsFile.is_open())
	{
		//Create a var to bookmark locations in the buffer
		int iBookmark[2] = { 0, 0 };

		//Go to end of file to find its length
		m_fsFile.seekg(0, m_fsFile.end);
		//Store the length of file
		std::streamoff iNumOfChars = m_fsFile.tellg();
		//Create a buffer that is big enough to hold all chars
		char * pBuff = new char[iNumOfChars];
		//Go back to begining of file
		m_fsFile.seekg(0, m_fsFile.beg);
		//Read file into buffer
		m_fsFile.read(pBuff, iNumOfChars);

		//For every char in the buffer
		for (int i = 0; i < strlen(pBuff); i++)
		{
			//Check if it is the line ending
			if (pBuff[i] == '\n' || pBuff[i] == '\0')
			{
				//If it is line ending then store it as the end location
				iBookmark[1] = i;
				
				//Create a pointer that is big enough to hold all the characters between start and end of line
				char* pLine = new char[((iBookmark[1] - iBookmark[0]) + 1)]; //Number of characters between the start of line and end, add one byte for the line terminator '\0'
				//Use memcpy to transfare line from buffer to a seperate pointer
				memcpy(pLine, &(pBuff[iBookmark[0]]), (iBookmark[1] - iBookmark[0])); //(into line pointer), (from location of first bookmark), (for length of line)
				//Add the terminating character '\0' as this isnt copied from buffer as we no longer want '\n'
				pLine[(iBookmark[1] - iBookmark[0])] = '\0';
				//Add pointer to the vector
				m_pLines.push_back(pLine);
				//Set the first bookmark to one after the end of last line
				iBookmark[0] = (i + 1);
				//Set pLine to NULL
				pLine = NULL;
			}
		}
		//Clean up buffer ect
		delete[] pBuff;
		pBuff = NULL;
		m_fsFile.close();

		return true;
	}

	return false;
}

bool CFileManager::ReadClassifications()
{
	//Check each line to see if it is a decleration of a classification
	//if it is start store the bookmark
	//if its the end then store the bookmark fillout struct and store in vector
	//clean up if need be

	//Bookmark to store start and end location of classifications
	int iBookmark[2] = { 0, 0 };

	//For everyline stored
	for (UINT i = 0; i < m_pLines.size(); i++)
	{
		OutputDebugStringA(m_pLines[i]);
		OutputDebugStringA("\n");

		//If the first char is the opening char '<' and the second char is not a '/' we know its an opening
		if (m_pLines[i][0] == '<' && m_pLines[i][1] != '/')
		{
			iBookmark[0] = i; //Store line number as bookmark
		}
		//else if the first char is '<' and second is a '/' then we know its closing and we will already have the start bound so we can now fill out the struct
		else if (m_pLines[i][0] == '<' && m_pLines[i][1] == '/')
		{
			iBookmark[1] = i;

			//Fillout classification struct to store
			Classification* pCls = new Classification;
			pCls->iLineBounds[0] = iBookmark[0];
			pCls->iLineBounds[1] = iBookmark[1];
			pCls->iNumOfData = (iBookmark[1] - iBookmark[0] - 1); //Self explanatory, the -1 offsets for the with number of actual data so the classification definiations arent included
			pCls->sName = m_pLines[pCls->iLineBounds[0]];
			pCls->sName.pop_back();
			pCls->sName.erase(pCls->sName.begin()); //erase the '<'

			m_Classifications.push_back(pCls);
		}
	}

	return true;
}

bool CFileManager::ReadData()
{
	//For each classification get line bounds
	//Read lines between bounds
	//Store name, data and classification it belongs to
	//Clean up if need be

	int iBookmark[2] = { 0, 0 };

	//For every classification
	for (UINT i = 0; i < m_Classifications.size(); i++)
	{
		//For every line contained by the classification
		for (int x = m_Classifications[i]->iLineBounds[0]; x < m_Classifications[i]->iLineBounds[1]; x++)
		{
			//For every char in line
			for (int y = 0; y < strlen(m_pLines[x]); y++)
			{
				//Check of any of them are '='
				if (m_pLines[x][y] == '=')
				{
					//If it was store the pos and then break from for loop
					iBookmark[1] = y;
					
					//Create data struct to store in vector
					Data* pData = new Data;

					//Store the name of the classification
					pData->sClassification = m_Classifications[i]->sName;

					//Store the name of the peice of data
					pData->sName = m_pLines[x];
					pData->sName.erase(iBookmark[1]);

					//Store the value of the peice of data
					pData->sValue = m_pLines[x];
					pData->sValue.erase(0, (iBookmark[1]+1));

					m_Data.push_back(pData);

					break;
				}
			}
		}
	}

	return true;
}

std::vector<Data*> CFileManager::GetData() const
{
	return m_Data;
}

bool CFileManager::UpdateData(std::vector<Data*>& data)
{
	//For every peice of data passed
	for (UINT i = 0; i < data.size(); i++)
	{
		//Ensure that it will not go out of bounds of m_Data
		if (i >= m_Data.size())
			break;

		//Find the same data in the data array
		for (UINT x = 0; x < m_Data.size(); x++)
		{
			//Ensure that it will not go out of bounds of data
			if (x >= data.size())
				break;

			//Check if its same peice then update
			if (data[i]->sClassification == m_Data[x]->sName && data[i]->sName == m_Data[x]->sName)
				m_Data[x]->sValue = data[i]->sValue;
		}
	}

	//Check of file is open if it is close it
	if (m_fsFile.is_open())
		m_fsFile.close();

	//Reopen as write
	m_fsFile.open(m_sFilePath, std::fstream::out);

	for (UINT i = 0; i < m_Classifications.size(); i++)
	{
		m_fsFile << '<' << m_Classifications[i]->sName << '>' << '\n';
		for (UINT x = 0; x < m_Data.size(); x++)
		{
			if (m_Classifications[i]->sName == m_Data[x]->sClassification)
			{
				m_fsFile << m_Data[x]->sName << '=' << m_Data[x]->sValue << '\n';
			}
		}

		m_fsFile << '<' << '/' << m_Classifications[i]->sName << '>' << '\n';
	}

	m_fsFile << "- File End -";

	m_fsFile.close();

	return true;
}

void CFileManager::Destroy()
{
	//Close file
	if (m_fsFile.is_open())
		m_fsFile.close();

	//Clear old data
	if (m_pLines.size() > 0)
	{
		for (UINT i = 0; i < m_pLines.size(); i++)
		{
			SafeDelete(m_pLines[i]);
		}

		m_pLines.clear();
	}

	if (m_Classifications.size() > 0)
	{
		for (UINT i = 0; i < m_Classifications.size(); i++)
		{
			SafeDelete(m_Classifications[i]);
		}

		m_Classifications.clear();
	}

	if (m_Data.size() > 0)
	{
		for (UINT i = 0; i < m_Data.size(); i++)
		{
			SafeDelete(m_Data[i]);
		}

		m_Data.clear();
	}

	//Reset vars
	m_sFilePath = "";
	m_iDumpNumber = 0;
		
	return;
}