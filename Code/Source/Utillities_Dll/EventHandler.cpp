#include <Include\Utillities_Dll\stdafx.h>
#include <Include\Utillities_Dll\EventHandler.h>

CEventHandler* CEventHandler::m_pInstance = new CEventHandler();

CEventHandler::CEventHandler():
m_fMouseX(0.0f),
m_fMouseY(0.0f),
m_fMousePrevX(0.0f),
m_fMousePrevY(0.0f)
{
	return;
}

CEventHandler::~CEventHandler()
{
	return;
}

bool CEventHandler::RegisterListener(IEventListener* pEventListener)
{
	if (m_pEventListeners.size() > 0)
	{
		for (UINT i = 0; i < m_pEventListeners.size(); i++)
		{
			if (m_pEventListeners[i] == pEventListener)
				return false;
		}

		m_pEventListeners.push_back(pEventListener);
		return true;
	}
	else
	{
		m_pEventListeners.push_back(pEventListener);
		return true;
	}

	return false;
}

bool CEventHandler::UnregisterListener(IEventListener* pEventListener)
{
	if (m_pEventListeners.size() > 0)
	{
		for (UINT i = 0; i < m_pEventListeners.size(); i++)
		{
			if (m_pEventListeners[i] == pEventListener)
			{
				m_pEventListeners.erase((m_pEventListeners.begin() + i));
				return true;
			}
		}

		return false;
	}
	else
	{
		return false;
	}

	return false;
}

void CEventHandler::PassWindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	if (m_pEventListeners.size() >= 0)
	{
		switch (iMsg)
		{
			default:
			{
				for (UINT i = 0; i < m_pEventListeners.size(); i++)
				{
					m_pEventListeners[i]->WindowEvent(hWnd, iMsg, wParam, lParam);
				}

				break;
			}

			case WM_MOUSEMOVE:
			{
				m_fMouseX = GET_X_LPARAM(lParam);
				m_fMouseY = GET_Y_LPARAM(lParam);

				if (wParam == MK_LBUTTON)
				{
					for (UINT i = 0; i < m_pEventListeners.size(); i++)
					{
						m_pEventListeners[i]->MouseEvent(hWnd, m_fMouseX, m_fMousePrevY, -1*(m_fMouseX - m_fMousePrevX), -1*(m_fMouseY - m_fMousePrevY), true, false);
					}

					m_fMousePrevX = m_fMouseX;
					m_fMousePrevY = m_fMouseY;

					break;
				}
				else if (wParam == MK_RBUTTON)
				{
					for (UINT i = 0; i < m_pEventListeners.size(); i++)
					{
						m_pEventListeners[i]->MouseEvent(hWnd, m_fMouseX, m_fMousePrevY, -1*(m_fMouseX - m_fMousePrevX), -1*(m_fMouseY - m_fMousePrevY), false, true);
					}

					m_fMousePrevX = m_fMouseX;
					m_fMousePrevY = m_fMouseY;

					break;
				}

				m_fMousePrevX = m_fMouseX;
				m_fMousePrevY = m_fMouseY;

				break;
			}
		}
	}

	return;
}

CEventHandler* CEventHandler::GetInstance()
{
	return m_pInstance;
}

DLL_EXPORT_IMPORT LRESULT CEventHandler::WndProc(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	CEventHandler::GetInstance()->PassWindowEvent(hWnd, iMsg, wParam, lParam);

	return DefWindowProc(hWnd, iMsg, wParam, lParam);
}
