#include <Include\Utillities_Dll\stdafx.h>
#include <Include\Utillities_Dll\Console.h>

CConsole::CConsole()
{
	m_hWnd = NULL;
	m_hWndParent = NULL;
	m_bMaximized = false;
	m_bMinimized = false;
	m_bVisable = false;
	m_dHeightScaleFactor = 0.2f;
	m_dWidthScaleFactor = 1.0f;

	m_iHeight = 800;
	m_iWidth = 800;
	m_iXPos = 0;
	m_iYPos = 0;

	m_pFontData = new TEXTMETRIC();

	m_iScrollPos = 0;
	m_iMaxLinesToDraw = 0;
	m_iOutputWidth = 0;

	m_bCapitalize = false;
	m_ssInput << "<< ";
	m_iCommandHistoryPos = 0;

	return;
}

CConsole::~CConsole()
{
	return;
}

bool CConsole::Create(HWND hParentWnd)
{
	m_hWndParent = hParentWnd;
	m_pSettings = CSettings::GetInstance();

	//Get settings from console file
	//Add some method to name console and search settings file for specific console, also some way for assigning keys ect so F1 opens one console F2 another...
	m_dHeightScaleFactor = m_pSettings->GetVar("ConsoleParams", "dHeightScaleFactor").d;
	m_dWidthScaleFactor = m_pSettings->GetVar("ConsoleParams", "dWidthScaleFactor").d;

	//Create window class
	WNDCLASSEX wndClsEx; ZeroMemory(&wndClsEx, sizeof(wndClsEx));
	wndClsEx.cbSize = sizeof(wndClsEx);
	wndClsEx.hbrBackground = (HBRUSH)(COLOR_WINDOW+4);
	wndClsEx.hCursor = LoadIcon(GetModuleHandle(NULL), IDC_ARROW);
	wndClsEx.hInstance = GetModuleHandle(NULL);
	wndClsEx.lpfnWndProc = WindowProcedure;
	wndClsEx.lpszClassName = L"consolewnd";
	wndClsEx.style = CS_VREDRAW | CS_HREDRAW;

	//Register window class
	if (FAILED(RegisterClassEx(&wndClsEx)))
	{
		return false;
	}

	//Get the X and Y pos of the parent window relative to the desktop for the pos of console, if there is a parent window
	if (m_hWndParent)
	{
		RECT rc; ZeroMemory(&rc, sizeof(RECT));
		GetClientRect(m_hWndParent, &rc);

		//Get height of parent window (main window) (needs recoding to get parent window)
		m_iHeight = (int)(((rc.bottom - rc.top) * m_dHeightScaleFactor));
		m_iWidth = (int)(((rc.right - rc.left) * m_dWidthScaleFactor));

 		MapWindowPoints(m_hWndParent, HWND_DESKTOP, (LPPOINT)(&rc), 2);

		m_iXPos = rc.left;
		m_iYPos = rc.top;

		//Create window
		m_hWnd = CreateWindowA("consolewnd", "Console Window", WS_POPUPWINDOW | WS_VSCROLL, m_iXPos, m_iYPos, m_iWidth, m_iHeight, m_hWndParent, NULL, GetModuleHandle(NULL), NULL);
	}
	else
	{
		//Needs developing in some way so that if there is no parent window the console behaves differently
		//Create window
		m_hWnd = CreateWindowA("consolewnd", "Console Window", WS_OVERLAPPEDWINDOW | WS_VSCROLL, m_iXPos, m_iYPos, m_iHeight, m_iWidth, m_hWndParent, NULL, GetModuleHandle(NULL), NULL);
	}

	//Register this class to recive events
	if (!CEventHandler::GetInstance()->RegisterListener(this))
		return false;

	//Check if window was created
	if (!m_hWnd)
		return false;

	//Retrive text metrics, store in a pointer
	HDC dc = GetDC(m_hWnd);
	GetTextMetrics(dc, m_pFontData);
	SetTextColor(dc, RGB(255, 255, 255));
	SetBkColor(dc, RGB(0, 0, 0));
	ReleaseDC(m_hWnd, dc);

	//Calculate max lines
	m_iMaxLinesToDraw = (m_iHeight / m_pFontData->tmHeight) - 2;

	//Calculate the output width as the width of the console includes the width of the scroll bar also remove the width of one max character to ensure that no characters are cut out
	m_iOutputWidth = m_iWidth - (GetSystemMetrics(SM_CXVSCROLL) + m_pFontData->tmMaxCharWidth);

	//Init scrollbar
	SCROLLINFO si; ZeroMemory(&si, sizeof(SCROLLINFO));
	si.fMask = SIF_ALL | SIF_DISABLENOSCROLL;
	si.cbSize = sizeof(SCROLLINFO);
	si.nMin = 1;
	si.nMax = static_cast<int>(m_sLinesToDraw.size() - 1);
	si.nPage = m_iMaxLinesToDraw;
	si.nPos = m_iScrollPos;
	si.nTrackPos = m_iScrollPos;

	SetScrollInfo(m_hWnd, SB_VERT, &si, true);

	return true;
}

void CConsole::Resize()
{
	RECT rc; ZeroMemory(&rc, sizeof(RECT));
	GetClientRect(m_hWndParent, &rc);

	//Get height of parent window (main window) (needs recoding to get parent window)
	m_iHeight = (int)(((rc.bottom - rc.top) * m_dHeightScaleFactor));
	m_iWidth = (int)(((rc.right - rc.left) * m_dWidthScaleFactor));

	MapWindowPoints(m_hWndParent, HWND_DESKTOP, (LPPOINT)(&rc), 2);

	m_iXPos = rc.left;
	m_iYPos = rc.top;

	SetWindowPos(m_hWnd, HWND_TOP, m_iXPos, m_iYPos, m_iWidth, m_iHeight, SWP_NOACTIVATE);

	//Update scrollbar
	UpdateScrollBar();
	//Reformat lines
	ReformatConsoleOutput();

	return;
}

void CConsole::Update()
{
	return;
}

void CConsole::Draw()
{
	if (m_hWnd)
	{
		HDC dc = GetDC(m_hWnd);
		SetTextColor(dc, RGB(255, 255, 255));
		SetBkColor(dc, RGB(0, 0, 0));
		if (m_sLinesToDraw.size() > 0)
		{
			//Draw lall the lines between the scroll position and the max lines to draw
			for (UINT i = (m_iScrollPos - 1); i < m_sLinesToDraw.size(); i++)
			{
				//As we dont want to draw past the window as it achives nothing
				if (i - (m_iScrollPos - 1) > m_iMaxLinesToDraw)
					break;

				//Output the line...
				TextOutA(dc, 0, ((i - (m_iScrollPos - 1)) * m_pFontData->tmHeight), m_sLinesToDraw[i].c_str(), static_cast<int>(m_sLinesToDraw[i].size()));
			}
		}

		//Output the input lines
		TextOutA(dc, 0, m_iHeight - (m_pFontData->tmHeight + 3), m_ssInput.str().c_str(), static_cast<int>(m_ssInput.str().length()));

		//Clean up
		ReleaseDC(m_hWnd, dc);
	}

	return;
}

void CConsole::Destroy()
{
	SafeDelete(m_pFontData);

	//Unregister all if any child windows
	if (m_ChildWindows.size() > 0)
	{
		for (UINT i = 0; i < m_ChildWindows.size(); i++)
		{
			UnregisterChildWindow(m_ChildWindows[i]);
		}
	}

	m_pSettings->SetVar("ConsoleParams", "dHeightScaleFactor", m_dHeightScaleFactor);
	m_pSettings->SetVar("ConsoleParams", "dWidthScaleFactor", m_dWidthScaleFactor);

	m_pSettings->SaveSettings();

	CEventHandler::GetInstance()->UnregisterListener(this);

	return;
}

void CConsole::WriteToConsole(std::string sOut)
{
	std::stringstream ssOut;
	ssOut << "Console: " << sOut << "\n";
	OutputDebugStringA(ssOut.str().c_str());

	//add to raw lines vector
	m_sRawLines.push_back(sOut);
	//add the raw line to the lines to draw vector, through the format line function
	FormatLine(sOut);
	//update scroll info
	UpdateScrollBar();

	return;
}

void CConsole::AddToInput(const char cCharacter)
{
	if (cCharacter != '\n')
	{
		m_ssInput << cCharacter;
	}

	return;
}

void CConsole::FormatLine(std::string sLine)
{
	SIZE size; //Store the width and height of the text
	HDC dc = GetDC(m_hWnd); //Get the device context ready to get the width and length of the string
	GetTextExtentPoint32A(dc, sLine.c_str(), static_cast<int>(sLine.size()), &size); //Get the width and length of the string

	//While the lenght of the string is greate then the width of the output area (calculated in the setup or the UpdateScrollBar Function)
	while (size.cx > m_iOutputWidth)
	{
		std::string sTemp; //Create a string to store the var passed as an intermediate while we edit the string
		for (UINT i = (m_iOutputWidth / m_pFontData->tmMaxCharWidth); i < sLine.size(); i++)
		{
			sTemp = sLine; //Each iteration we add one more character from the line so reset it to the other line then...
			sTemp.erase((sTemp.begin() + i), (sTemp.end() - 1)); //Erase from that point
			GetTextExtentPoint32A(dc, sTemp.c_str(), static_cast<int>(sTemp.size()), &size); //Then get the width of the temp string

			//If the width of the temp string is greater then that of the output area we know that we need to add this string minus one character to the lines to draw then erase this part from the original string
			if (size.cx > m_iOutputWidth)
			{
				//Remove last character
				sTemp.pop_back();

				//Check if first character is white space if it is remove it
				if (sTemp[0] == ' ')
					sTemp.erase(0, 1);

				//Add the temp line to the lines to draw vector
				m_sLinesToDraw.push_back(sTemp);

				//Increase the scroll pos as a line was added, this keeps it at bottom if the user is already at the bottom
				UpdateScrollPos(1);

				//Erase from the start of the string to the point that the string exceeded the width of the output area
				sLine.erase(0, (i));

				//Check if first character is white space if it is remove it
				if (sLine[0] == ' ')
					sLine.erase(0, 1);

				break;
			}
		}

		//Get the width of the original line minus the changes to see if it needs another go around to remove some more of the string
		GetTextExtentPoint32A(dc, sLine.c_str(), static_cast<int>(sLine.size()), &size);
	}
	//No longer need dc so release it
	ReleaseDC(m_hWnd, dc);

	//Whatever is in sLine is now short enough that non needs to be removed so add it to the vector (may need to add some form of check to see if sLine actually contains some characters that arent whitespace
	m_sLinesToDraw.push_back(sLine);
	//Increase the scroll pos as a line was added, this keeps it at bottom if the user is already at the bottom
	UpdateScrollPos(1);

	return;
}

inline void CConsole::ReformatConsoleOutput()
{
	//Clear the vector and reset capactity to 0
	m_sLinesToDraw.clear();
	m_sLinesToDraw.shrink_to_fit();

	//Go through all the raw lines amd have them reformatted to fit the resized window !
	for (UINT i = 0; i < m_sRawLines.size(); i++)
	{
		FormatLine(m_sRawLines[i]);
	}

	return;
}

inline void CConsole::UpdateScrollPos(int iOffset)
{
	//Add the offset but first ensure that there are more lines then the console can draw, this stops the other lines moving
	//Off the screen when new lines are added
	if(m_sLinesToDraw.size() > m_iMaxLinesToDraw)
		m_iScrollPos += iOffset;

	//Ensure its limited to the scroll bars max, minus the max lines to draw
	if (m_iScrollPos > (static_cast<unsigned int>(m_sLinesToDraw.size()) - m_iMaxLinesToDraw))
		m_iScrollPos = static_cast<unsigned int>(m_sLinesToDraw.size()) - m_iMaxLinesToDraw;

	//Ensure its limited to be greater then the scroll bars min
	if (m_iScrollPos < 1)
		m_iScrollPos = 1;

	return;
}

inline void CConsole::ParaseInput()
{
	std::string sCommand = m_ssInput.str();
	sCommand.erase(0, 3);

	if (sCommand.find_first_not_of(' ') == sCommand.npos)
	{
		m_ssInput.str(std::string());
		m_ssInput << "<< ";

		//Redraw
		RECT rc; ZeroMemory(&rc, sizeof(RECT));
		GetClientRect(m_hWndParent, &rc);
		InvalidateRect(m_hWnd, &rc, true);

		return;
	}

	std::stringstream ssConsoleOut;
	ssConsoleOut << "INPUT: " << sCommand;
	WriteToConsole(ssConsoleOut.str());

	m_sCommandHistory.push_back(sCommand);
	m_iCommandHistoryPos = static_cast<int>(m_sCommandHistory.size());

	m_ssInput.str(std::string());
	m_ssInput << "<< ";

	sCommand.erase(remove(sCommand.begin(), sCommand.end(), ' '), sCommand.end());

	if (sCommand == "?")
	{
		PrintConsoleHelp();

		return;
	}

	for (UINT i = 0; i < sCommand.length(); i++)
	{
		if (sCommand[i] == '(' && sCommand[i+1] == ')')
		{
			sCommand.erase(i, 2); //Erase the brackets !

			CallConsoleFunction(sCommand);

			//Redraw
			RECT rc; ZeroMemory(&rc, sizeof(RECT));
			GetClientRect(m_hWndParent, &rc);
			InvalidateRect(m_hWnd, &rc, true);

			return;
		}
		else if (sCommand[i] == '=')
		{
			std::string sName = sCommand;
			sName.erase(sName.begin() + i, sName.end());
			std::string sValue = sCommand;
			sValue.erase(0, i+1);

			SetConsoleVariable(sName, sValue);

			//Redraw
			RECT rc; ZeroMemory(&rc, sizeof(RECT));
			GetClientRect(m_hWndParent, &rc);
			InvalidateRect(m_hWnd, &rc, true);

			return;
		}
	}

	if (!RetriveConsoleVariable(sCommand))
		WriteToConsole("Not a recognized fromat, please enter '?' for help !");


	//Redraw
	RECT rc; ZeroMemory(&rc, sizeof(RECT));
	GetClientRect(m_hWndParent, &rc);
	InvalidateRect(m_hWnd, &rc, true);

	return;
}

inline void CConsole::PrintConsoleHelp()
{
	WriteToConsole("Console Help !");

	WriteToConsole("To call a console function, use the syntax:");
	WriteToConsole("FunctionName()");

	WriteToConsole("To retrive the value of a variable, use the syntax:");
	WriteToConsole("VarName");

	WriteToConsole("To set the value of a variable, use the syntax:");
	WriteToConsole("VarName = Value");

	WriteToConsole("Here is a list of all the registered console functions:");

	for (UINT i = 0; i < m_ConsoleFunctions.size(); i++)
	{
		std::stringstream ss;
		ss << m_ConsoleFunctions[i].sName << "() - " << m_ConsoleFunctions[i].sDesc;
		WriteToConsole(ss.str());
	}

	WriteToConsole("Here is a list of all the registered console variables:");

	for (UINT i = 0; i < m_ConsoleVariables.size(); i++)
	{
		std::stringstream ss;
		ss << m_ConsoleVariables[i].sName << " - " << m_ConsoleVariables[i].sDesc;
		WriteToConsole(ss.str());
	}

	return;
}

inline void CConsole::NavigateCommandHistory(int iOffset)
{
	m_iCommandHistoryPos += iOffset;

	if (m_iCommandHistoryPos < 0)
		m_iCommandHistoryPos = static_cast<int>(m_sCommandHistory.size());

	if (m_iCommandHistoryPos > static_cast<int>(m_sCommandHistory.size()))
		m_iCommandHistoryPos = 0;

	m_ssInput.str(std::string());
	m_ssInput << "<< ";

	if(m_iCommandHistoryPos != m_sCommandHistory.size())
		m_ssInput << m_sCommandHistory[m_iCommandHistoryPos];

	//Redraw
	RECT rc; ZeroMemory(&rc, sizeof(RECT));
	GetClientRect(m_hWndParent, &rc);
	InvalidateRect(m_hWnd, &rc, true);

	return;
}

void CConsole::CallConsoleFunction(std::string sName)
{
if (m_ConsoleFunctions.size() > 0)
{
	for (UINT i = 0; i < m_ConsoleFunctions.size(); i++)
	{
		if (m_ConsoleFunctions[i].sName == sName)
		{
			m_ConsoleFunctions[i].pFunc();
			return;
		}
	}

	WriteToConsole("Couldn't find function !");
}

return;
}

bool CConsole::RegisterConsoleFunction(std::string sName, void(*pFunc)(), std::string sDesc)
{
	if (m_ConsoleFunctions.size())
	{
		for (UINT i = 0; i < m_ConsoleFunctions.size(); i++)
		{
			if (m_ConsoleFunctions[i].sName == sName)
			{
				return false;
			}
		}
	}

	ConsoleFunctions cf; ZeroMemory(&cf, sizeof(ConsoleFunctions));
	cf.sName = sName;
	cf.sDesc = sDesc;
	cf.pFunc = pFunc;

	m_ConsoleFunctions.push_back(cf);

	return true;
}

bool CConsole::UnregisterConsoleFunction(std::string sName)
{
	if (m_ConsoleFunctions.size() < 0)
	{
		for (UINT i = 0; i < m_ConsoleFunctions.size(); i++)
		{
			if (m_ConsoleFunctions[i].sName == sName)
			{
				m_ConsoleFunctions.erase(m_ConsoleFunctions.begin() + i);
				return true;
			}
		}
	}

	return false;
}

void CConsole::SetConsoleVariable(std::string sName, std::string sValue)
{
	if (m_ConsoleVariables.size() > 0)
	{
		for (UINT i = 0; i < m_ConsoleVariables.size(); i++)
		{
			if (m_ConsoleVariables[i].sName == sName)
			{
				if (sName[0] == 'i')
				{
					*(int*)(m_ConsoleVariables[i].pData) = atoi(sValue.c_str());
					RetriveConsoleVariable(sName);

					return;
				}
				else if (sName[0] == 'f')
				{
					*(double*)(m_ConsoleVariables[i].pData) = atof(sValue.c_str());
					RetriveConsoleVariable(sName);

					return;
				}
				else if (sName[0] == 'b')
				{
					if (sValue == "true")
						*(bool*)(m_ConsoleVariables[i].pData) = true;
					else if (sValue == "false")
						*(bool*)(m_ConsoleVariables[i].pData) = false;
					else
						WriteToConsole("Value entered wasn't valid for boolean conversion please enter either \"true\" or \"false\" accordingly !");

					RetriveConsoleVariable(sName);

					return;
				}
			}
		}
	}

	WriteToConsole("Couldn't find variable !");
	return;
}

bool CConsole::RetriveConsoleVariable(std::string sName)
{
	if (m_ConsoleVariables.size() > 0)
	{
		for (UINT i = 0; i < m_ConsoleVariables.size(); i++)
		{
			if (m_ConsoleVariables[i].sName == sName)
			{
				std::stringstream ssOut;
				ssOut << "Variable ";
				if (sName[0] == 'i')
				{
					ssOut << sName << " = " << *(int*)(m_ConsoleVariables[i].pData);
					WriteToConsole(ssOut.str());
					return true;
				}
				else if (sName[0] == 'f')
				{
					ssOut << sName << " = " << *(double*)(m_ConsoleVariables[i].pData);
					WriteToConsole(ssOut.str());
					return true;
				}
				else if (sName[0] == 'b')
				{
					if (*(bool*)(m_ConsoleVariables[i].pData))
					{
						ssOut << sName << " = " << "true";
						WriteToConsole(ssOut.str());
						return true;
					}
					else
					{
						ssOut << sName << " = " << "false";
						WriteToConsole(ssOut.str());
						return true;
					}
				}
				else
				{
					break;
				}
			}
		}

		return false;
	}

	return false;
}

bool CConsole::RegisterConsoleVariable(std::string sName, void* pValue, std::string sDesc)
{
	if (m_ConsoleVariables.size() > 0)
	{
		for (UINT i = 0; i < m_ConsoleVariables.size(); i++)
		{
			if (m_ConsoleVariables[i].sName == sName)
			{
				return false;
			}
		}
	}

	ConsoleVariables cv; ZeroMemory(&cv, sizeof(ConsoleVariables));
	cv.sName = sName;
	cv.sDesc = sDesc;
	cv.pData = pValue;


	m_ConsoleVariables.push_back(cv);

	return false;
}

bool CConsole::UnregisterConsoleVariable(std::string sName)
{
	if (m_ConsoleVariables.size() < 0)
	{
		for (UINT i = 0; i < m_ConsoleVariables.size(); i++)
		{
			if (m_ConsoleVariables[i].sName == sName)
			{
				m_ConsoleVariables.erase(m_ConsoleVariables.begin() + i);
				return true;
			}
		}
	}

	return false;
}

HWND CConsole::GetHWND()
{
	return m_hWnd;
}

int CConsole::GetHeight()
{
	return m_iHeight;
}

int CConsole::GetWidth()
{
	return m_iWidth;
}

void CConsole::WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	if (hWnd == m_hWndParent) //If the window the message has come from is the parent window then...
	{
		switch (iMsg)
		{
			//...Check for key events as if F1 is pressed we want to display the window, this could be handled in CMainWindow but I feel its cleaner to do it here
			case WM_KEYDOWN:
				switch (LOWORD(wParam))
				{
					//If it is F1 pressed then toggle the visibility of the console and change the active window to always be the parent
					case VK_F1:
					{
						if (!m_bVisable)
						{
							ShowWindow(m_hWnd, SW_SHOW);
							SetActiveWindow(m_hWndParent);
							m_bVisable = true;
						}
						else
						{
							ShowWindow(m_hWnd, SW_HIDE);
							SetActiveWindow(m_hWndParent);
							m_bVisable = false;
						}
						break;
					}
				}

				break;
		}
		return;
	}
	else if (hWnd == m_hWnd) //If the window the message is from is this one then...
	{
		switch (iMsg)
		{
			//If it is the vertical scroll bar message... see for details https://msdn.microsoft.com/en-us/library/windows/desktop/bb787577(v=vs.85).aspx
			case WM_VSCROLL:
			{
				if (LOWORD(wParam) == SB_THUMBTRACK) //Check to see if the LOWORD is SB_THUMBTRACK which means the bar has been moved
				{
					m_iScrollPos = HIWORD(wParam); //Set the scroll pos to the HIWORD which is the new position
					//Now update the scroll bar
					UpdateScrollBar();
					break;
				}
				else if (LOWORD(wParam) == SB_LINEUP)
				{
					UpdateScrollPos(-1);
					UpdateScrollBar();
					break;
				}
				else if (LOWORD(wParam) == SB_LINEDOWN)
				{
					UpdateScrollPos(1);
					UpdateScrollBar();
					break;
				}
				break;
			}

			//Handle key events
			case WM_KEYDOWN:
			{
				//When a key is pressed update capitilize variable
				//If the low bit of the key VK_CAPITAL is true then it is toggled on if the hi bit of VK_SHIFT is true it is pressed down
				//If both are true then do not capitalize
				//If only one is true then capitalize
				//If none are true then do not capitalize
				if (GetKeyState(VK_CAPITAL) & 0x0001 && GetKeyState(VK_SHIFT) & 0x1000)
				{
					m_bCapitalize = false;
				}
				else if (GetKeyState(VK_CAPITAL) & 0x0001 || GetKeyState(VK_SHIFT) & 0x1000)
				{
					m_bCapitalize = true;
				}
				else
				{
					m_bCapitalize = false;
				}

				switch (LOWORD(wParam))
				{
					//If it is F1 pressed then toggle the visibility of the console and change the active window to always be the parent
					case VK_F1:
					{
						if (!m_bVisable)
						{
							ShowWindow(m_hWnd, SW_SHOW);
							SetActiveWindow(m_hWndParent);
							m_bVisable = true;
						}
						else
						{
							ShowWindow(m_hWnd, SW_HIDE);
							SetActiveWindow(m_hWndParent);
							m_bVisable = false;
						}
					}
					break;

				//Key handling for input to console!
					case VK_UP:
					{
						NavigateCommandHistory(-1);
					}
					break;

					case VK_DOWN:
					{
						NavigateCommandHistory(1);
					}
					break;

					case VK_RETURN:
					{
						ParaseInput();
					}
					break;

					case VK_BACK:
					{
						if (m_ssInput.str().length() > 3)
						{
							std::string sIntermidiate(m_ssInput.str());
							sIntermidiate.pop_back();
							m_ssInput.str(std::string());
							m_ssInput << sIntermidiate;

							//Redraw
							RECT rc; ZeroMemory(&rc, sizeof(RECT));
							GetClientRect(m_hWndParent, &rc);
							InvalidateRect(m_hWnd, &rc, true);

							break;
						}
					}
					break;

					case VK_OEM_COMMA:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							m_ssInput << '<';
						}
						else
						{
							m_ssInput << ',';
						}
					}
					break;

					case VK_OEM_PERIOD:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							m_ssInput << '>';
						}
						else
						{
							m_ssInput << '.';
						}
					}
					break;

					case VK_OEM_PLUS:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							m_ssInput << '+';
						}
						else
						{
							m_ssInput << '=';
						}
					}
					break;

					case VK_OEM_MINUS:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							m_ssInput << '_';
						}
						else
						{
							m_ssInput << '-';
						}
					}
					break;

					case VK_OEM_2: // '/''?' key
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							m_ssInput << '?';
						}
						else
						{
							m_ssInput << '/';
						}
					}
					break;

					case VK_SPACE:
					{
						m_ssInput << ' ';
					}
					break;
					case VK_KEY_A:
					{
						if (m_bCapitalize)
						{
							AddToInput('A');
							break;
						}
						else
						{
							AddToInput('a');
							break;
						}
					}
					break;

					case VK_KEY_B:
					{
						if (m_bCapitalize)
						{
							AddToInput('B');
							break;
						}
						else
						{
							AddToInput('b');
							break;
						}
					}
					break;

					case VK_KEY_C:
					{
						if (m_bCapitalize)
						{
							AddToInput('C');
							break;
						}
						else
						{
							AddToInput('c');
							break;
						}
					}
					break;

					case VK_KEY_D:
					{
						if (m_bCapitalize)
						{
							AddToInput('D');
							break;
						}
						else
						{
							AddToInput('d');
							break;
						}
					}
					break;

					case VK_KEY_E:
					{
						if (m_bCapitalize)
						{
							AddToInput('E');
							break;
						}
						else
						{
							AddToInput('e');
							break;
						}
					}
					break;

					case VK_KEY_F:
					{
						if (m_bCapitalize)
						{
							AddToInput('F');
							break;
						}
						else
						{
							AddToInput('f');
							break;
						}
					}
					break;

					case VK_KEY_G:
					{
						if (m_bCapitalize)
						{
							AddToInput('G');
							break;
						}
						else
						{
							AddToInput('g');
							break;
						}
					}
					break;

					case VK_KEY_H:
					{
						if (m_bCapitalize)
						{
							AddToInput('H');
							break;
						}
						else
						{
							AddToInput('h');
							break;
						}
					}
					break;

					case VK_KEY_I:
					{
						if (m_bCapitalize)
						{
							AddToInput('I');
							break;
						}
						else
						{
							AddToInput('i');
							break;
						}
					}
					break;

					case VK_KEY_J:
					{
						if (m_bCapitalize)
						{
							AddToInput('J');
							break;
						}
						else
						{
							AddToInput('j');
							break;
						}
					}
					break;

					case VK_KEY_K:
					{
						if (m_bCapitalize)
						{
							AddToInput('K');
							break;
						}
						else
						{
							AddToInput('k');
							break;
						}
					}
					break;

					case VK_KEY_L:
					{
						if (m_bCapitalize)
						{
							AddToInput('L');
							break;
						}
						else
						{
							AddToInput('l');
							break;
						}
					}
					break;

					case VK_KEY_M:
					{
						if (m_bCapitalize)
						{
							AddToInput('M');
							break;
						}
						else
						{
							AddToInput('m');
							break;
						}
					}
					break;

					case VK_KEY_N:
					{
						if (m_bCapitalize)
						{
							AddToInput('N');
							break;
						}
						else
						{
							AddToInput('n');
							break;
						}
					}
					break;

					case VK_KEY_O:
					{
						if (m_bCapitalize)
						{
							AddToInput('O');
							break;
						}
						else
						{
							AddToInput('o');
							break;
						}
					}
					break;

					case VK_KEY_P:
					{
						if (m_bCapitalize)
						{
							AddToInput('P');
							break;
						}
						else
						{
							AddToInput('p');
							break;
						}
					}
					break;

					case VK_KEY_Q:
					{
						if (m_bCapitalize)
						{
							AddToInput('Q');
							break;
						}
						else
						{
							AddToInput('q');
							break;
						}
					}
					break;

					case VK_KEY_R:
					{
						if (m_bCapitalize)
						{
							AddToInput('R');
							break;
						}
						else
						{
							AddToInput('r');
							break;
						}
					}
					break;

					case VK_KEY_S:
					{
						if (m_bCapitalize)
						{
							AddToInput('S');
							break;
						}
						else
						{
							AddToInput('s');
							break;
						}
					}
					break;

					case VK_KEY_T:
					{
						if (m_bCapitalize)
						{
							AddToInput('T');
							break;
						}
						else
						{
							AddToInput('t');
							break;
						}
					}
					break;

					case VK_KEY_U:
					{
						if (m_bCapitalize)
						{
							AddToInput('U');
							break;
						}
						else
						{
							AddToInput('u');
							break;
						}
					}
					break;

					case VK_KEY_V:
					{
						if (m_bCapitalize)
						{
							AddToInput('V');
							break;
						}
						else
						{
							AddToInput('v');
							break;
						}
					}
					break;

					case VK_KEY_W:
					{
						if (m_bCapitalize)
						{
							AddToInput('W');
							break;
						}
						else
						{
							AddToInput('w');
							break;
						}
					}
					break;

					case VK_KEY_X:
					{
						if (m_bCapitalize)
						{
							AddToInput('X');
							break;
						}
						else
						{
							AddToInput('x');
							break;
						}
					}
					break;

					case VK_KEY_Y:
					{
						if (m_bCapitalize)
						{
							AddToInput('Y');
							break;
						}
						else
						{
							AddToInput('y');
							break;
						}
					}
					break;

					case VK_KEY_Z:
					{
						if (m_bCapitalize)
						{
							AddToInput('Z');
							break;
						}
						else
						{
							AddToInput('z');
							break;
						}
					}
					break;

					case VK_KEY_0:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput(')');
							break;
						}
						else
						{
							AddToInput('0');
							break;
						}
					}
					break;

					case VK_KEY_1:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('!');
							break;
						}
						else
						{
							AddToInput('1');
							break;
						}
					}
					break;

					case VK_KEY_2:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('"');
							break;
						}
						else
						{
							AddToInput('2');
							break;
						}
					}
					break;

					case VK_KEY_3:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('�');
							break;
						}
						else
						{
							AddToInput('3');
							break;
						}
					}
					break;

					case VK_KEY_4:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('$');
							break;
						}
						else
						{
							AddToInput('4');
							break;
						}
					}
					break;

					case VK_KEY_5:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('%');
							break;
						}
						else
						{
							AddToInput('5');
							break;
						}
					}
					break;

					case VK_KEY_6:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('^');
							break;
						}
						else
						{
							AddToInput('6');
							break;
						}
					}
					break;

					case VK_KEY_7:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('&');
							break;
						}
						else
						{
							AddToInput('7');
							break;
						}
					}
					break;

					case VK_KEY_8:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('*');
							break;
						}
						else
						{
							AddToInput('8');
							break;
						}
					}
					break;

					case VK_KEY_9:
					{
						if (GetKeyState(VK_SHIFT) & 0x1000)
						{
							AddToInput('(');
							break;
						}
						else
						{
							AddToInput('9');
							break;
						}
					}
					break;
				}
			}
			break;
		}
	}
	return;
}

inline void CConsole::UpdateScrollBar()
{
	//Calculate max lines
	m_iMaxLinesToDraw = (m_iHeight / m_pFontData->tmHeight) - 2;

	m_iOutputWidth = m_iWidth - (GetSystemMetrics(SM_CXVSCROLL) + m_pFontData->tmMaxCharWidth);

	//Set scroll bar info
	SCROLLINFO si; ZeroMemory(&si, sizeof(SCROLLINFO));
	si.fMask = SIF_ALL | SIF_DISABLENOSCROLL;
	si.cbSize = sizeof(SCROLLINFO);
	si.nMin = 1;
	si.nMax = static_cast<int>(m_sLinesToDraw.size() - 1);
	si.nPage = m_iMaxLinesToDraw;
	si.nPos = m_iScrollPos;
	si.nTrackPos = m_iScrollPos;

	SetScrollInfo(m_hWnd, SB_VERT, &si, true);

	//Redraw
	RECT rc; ZeroMemory(&rc, sizeof(RECT));
	GetClientRect(m_hWndParent, &rc);
	InvalidateRect(m_hWnd, &rc, true);

	return;
}