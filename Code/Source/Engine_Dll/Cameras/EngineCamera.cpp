#include <Include\Engine_Dll\stdafx.h>
#include <Include\Engine_Dll\Cameras\EngineCamera.h>

CEngineCamera::CEngineCamera():
ICamera(NULL, nullptr, nullptr, nullptr),
m_bCreated(false),
m_fCameraSpeed(3000),
m_fCameraRotationSpeedRad(2 * XM_PI)
{
	return;
}

CEngineCamera::CEngineCamera(HWND hWnd, CTimer* pTimer, CSettings * pSettings, CConsole * pConsole):
ICamera(hWnd, pTimer, pSettings, pConsole),
m_bCreated(false),
m_fCameraSpeed(3000),
m_fCameraRotationSpeedRad(2 * XM_PI),
m_fFOV(1.13446f)
{
	

	return;
}

CEngineCamera::~CEngineCamera()
{
	return;
}

void CEngineCamera::MouseEvent(HWND hWnd, float fX, float fY, float fDeltaX, float fDeltaY, bool bLeftClick, bool bRightClick)
{
	if (m_hMainWnd == hWnd)
	{
		if (bRightClick)
		{
			float xRatio = fDeltaX / (fDeltaX + fDeltaY);
			float yRatio = fDeltaY / (fDeltaY + fDeltaX);

			m_xmvCameraRotRad += XMVectorSet(fDeltaY * m_fCameraRotationSpeedRad * m_pSystemTimer->GetDeltaT(), fDeltaX * m_fCameraRotationSpeedRad * m_pSystemTimer->GetDeltaT(), 0.0f, 0.0f);

			if (m_xmvCameraRotRad.m128_f32[0] > XM_PIDIV2)
			{
				m_xmvCameraRotRad.m128_f32[0] = XM_PIDIV2;
			}
			else if (m_xmvCameraRotRad.m128_f32[0] < -XM_PIDIV2)
			{
				m_xmvCameraRotRad.m128_f32[0] = -XM_PIDIV2;
			}

			m_xmvCameraTarget = CalculateCameraTarget();

			return;
		}
	}

	return;
}

void CEngineCamera::WindowEvent(HWND hWnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	if (m_hMainWnd == hWnd)
	{
		switch (iMsg)
		{
			default:
				break;

			case WM_KEYDOWN:
			{
				switch (LOWORD(wParam))
				{
					default:
						break;

					case VK_UP:
					{
						m_xmvCameraRotRad += XMVectorSet(m_fCameraRotationSpeedRad * m_pSystemTimer->GetDeltaT(), 0.0f, 0.0f, 0.0f);
						
						if (m_xmvCameraRotRad.m128_f32[0] > XM_PIDIV2)
						{
							m_xmvCameraRotRad.m128_f32[0] = XM_PIDIV2;
						}

						m_xmvCameraTarget = CalculateCameraTarget();
						break;
					}

					case VK_DOWN:
					{
						m_xmvCameraRotRad += XMVectorSet(- 1 * m_fCameraRotationSpeedRad * m_pSystemTimer->GetDeltaT(), 0.0f, 0.0f, 0.0f);
						
						if (m_xmvCameraRotRad.m128_f32[0] < -XM_PIDIV2)
						{
							m_xmvCameraRotRad.m128_f32[0] = -XM_PIDIV2;
						}

						m_xmvCameraTarget = CalculateCameraTarget();
						break;
					}

					case VK_RIGHT:
					{
						m_xmvCameraRotRad += XMVectorSet(0.0f, m_fCameraRotationSpeedRad * m_pSystemTimer->GetDeltaT(), 0.0f, 0.0f);
						m_xmvCameraTarget = CalculateCameraTarget();
						break;
					}

					case VK_LEFT:
					{
						m_xmvCameraRotRad += XMVectorSet(0.0f, -1 * m_fCameraRotationSpeedRad * m_pSystemTimer->GetDeltaT(), 0.0f, 0.0f);
						m_xmvCameraTarget = CalculateCameraTarget();
						break;
					}

					case VK_KEY_W:
					{
						float speed = (float)(m_fCameraSpeed *  m_pSystemTimer->GetDeltaT());
						float x, y, z;
						x = speed * sin(m_xmvCameraRotRad.m128_f32[1]);
						y = speed * sin(m_xmvCameraRotRad.m128_f32[0]);
						z = speed * cos(m_xmvCameraRotRad.m128_f32[1]);
						m_xmvCameraPos += XMVectorSet(x, y, z, 0.0f);
						m_xmvCameraTarget = CalculateCameraTarget();
						break;
					}

					case VK_KEY_S:
					{
						float speed = (-1 * m_fCameraSpeed * m_pSystemTimer->GetDeltaT());
						float x, y, z;
						x = speed * sin(m_xmvCameraRotRad.m128_f32[1]);
						y = speed * sin(m_xmvCameraRotRad.m128_f32[0]);
						z = speed * cos(m_xmvCameraRotRad.m128_f32[1]);
						m_xmvCameraPos += XMVectorSet(x, y, z, 0.0f);
						m_xmvCameraTarget = CalculateCameraTarget();
						break;
					}

					case VK_KEY_A:
					{
						float speed = (-1 * m_fCameraSpeed * m_pSystemTimer->GetDeltaT());
						float x, z;
						x = speed * cos(m_xmvCameraRotRad.m128_f32[1]);
						z = speed * -sin(m_xmvCameraRotRad.m128_f32[1]);
						m_xmvCameraPos += XMVectorSet(x, 0.0f, z, 0.0f);
						m_xmvCameraTarget = CalculateCameraTarget();
						break;
					}

					case VK_KEY_D:
					{
						float speed = (m_fCameraSpeed *  m_pSystemTimer->GetDeltaT());
						float x, z;
						x = speed * cos(m_xmvCameraRotRad.m128_f32[1]);
						z = speed * -sin(m_xmvCameraRotRad.m128_f32[1]);
						m_xmvCameraPos += XMVectorSet(x, 0.0f, z, 0.0f);
						m_xmvCameraTarget = CalculateCameraTarget();
						break;
					}
				}
				break;
			}
		}

		return;
	}

	return;
}

XMVECTOR CEngineCamera::CalculateCameraTarget()
{
	XMVECTOR xmvRet = m_xmvCameraPos;

	xmvRet.m128_f32[2] += cos(m_xmvCameraRotRad.m128_f32[1]);
	xmvRet.m128_f32[1] += sin(m_xmvCameraRotRad.m128_f32[0]);
	xmvRet.m128_f32[0] += sin(m_xmvCameraRotRad.m128_f32[1]);

	return xmvRet;
}

bool CEngineCamera::Create()
{
	if (!m_bCreated)
	{
		m_pMainConsole->RegisterConsoleVariable("fFOV", &m_fFOV);
		m_pMainConsole->RegisterConsoleVariable("fNear", &m_fNear);
		m_pMainConsole->RegisterConsoleVariable("fFar", &m_fFar);
		m_pMainConsole->RegisterConsoleVariable("fCameraSpeed", &m_fCameraSpeed);
		m_pMainConsole->RegisterConsoleVariable("fCameraRotationSpeedRad", &m_fCameraRotationSpeedRad);

		m_xmvUp = XMVectorSet(0.0f, 1.0f, 0.0f, 1.0f);

		RECT rc; ZeroMemory(&rc, sizeof(RECT));
		GetWindowRect(m_hMainWnd, &rc);

		//Fill in some initial values, retrive these from a settings file
		m_xmvCameraPos = XMVectorSet(0.0f, 32.0f, 0.0f, 1.0f);
		m_xmvCameraRotRad = XMVectorSet(0.0f, 0.0f, 0.0f, 0.0f);
		m_xmvCameraTarget = CalculateCameraTarget();
		m_fNear = 1.0f;
		m_fFar = 10.0f;
		//Create view and proj matracies
		XMStoreFloat4x4(&m_xmf4x4View, XMMatrixLookAtLH(m_xmvCameraPos, m_xmvCameraTarget, m_xmvUp));
		XMStoreFloat4x4(&m_xmf4x4Proj, XMMatrixPerspectiveFovLH(XM_PIDIV4, (float)(rc.right - rc.left) / (float)(rc.bottom - rc.top), 1.0f, 1000.0f));

		//Register event listener, wont be necessary when I create input system
		CEventHandler::GetInstance()->RegisterListener(this);

		return ICamera::Create();
	}

	return ICamera::Create();
}

void CEngineCamera::Update()
{
	/*RECT rc; ZeroMemory(&rc, sizeof(RECT));
	GetWindowRect(m_hMainWnd, &rc);*/

	XMStoreFloat4x4(&m_xmf4x4View, XMMatrixLookAtLH(m_xmvCameraPos, m_xmvCameraTarget, m_xmvUp));
	/*XMStoreFloat4x4(&m_xmf4x4Proj, XMMatrixPerspectiveFovLH(m_fFOV, (float)(rc.right - rc.left) / (float)(rc.bottom - rc.top), m_fNear, 1000.0f));*/

	return ICamera::Update();
}

void CEngineCamera::Shutdown()
{
	//Unregister event listener, wont be necessary when I create input system
	CEventHandler::GetInstance()->UnregisterListener(this);

	return ICamera::Shutdown();
}