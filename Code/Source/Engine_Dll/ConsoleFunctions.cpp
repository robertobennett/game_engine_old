#include <Include\Engine_Dll\stdafx.h>
#include <Include\Engine_Dll\ConsoleFunctions.h>

CConsoleFunctions* CConsoleFunctions::m_pInstance = CConsoleFunctions::GetInstance();

CConsoleFunctions::CConsoleFunctions():
m_hMainWindow(NULL),
m_pMainConsole(nullptr),
m_pRenderer(nullptr)
{
	return;
}

CConsoleFunctions::CConsoleFunctions(HWND hWnd, CConsole* pCsl, CRenderer* pRenderer) :
m_hMainWindow(hWnd),
m_pMainConsole(pCsl),
m_pRenderer(pRenderer)
{
	return;
}

CConsoleFunctions::~CConsoleFunctions()
{
	return;
}

bool CConsoleFunctions::RegisterFunctions()
{
	if (!m_pMainConsole->RegisterConsoleFunction("Exit", Exit, "Exits the engine !"))
		return false;

	if (!m_pMainConsole->RegisterConsoleFunction("SetRasterModeWireframe", SetRasterModeWireframe, "Sets the renderers raster state to wireframe with culling of back faces !"))
		return false;

	if (!m_pMainConsole->RegisterConsoleFunction("SetRasterModeWireframeNoCull", SetRasterModeWireframeNoCull, "Sets the renderers raster state to wireframe without culling of back faces !"))
		return false;

	if (!m_pMainConsole->RegisterConsoleFunction("SetRasterModeDefault", SetRasterModeDefault, "Sets the renderers raster state to the default raster state !"))
		return false;

	if (!m_pMainConsole->RegisterConsoleFunction("ResetRasterState", ResetRasterState, "Resets the raster state to use the state registered with the geometry !"))
		return false;

	return true;
}

bool CConsoleFunctions::UnregisterFunctions()
{
	if (!m_pMainConsole->UnregisterConsoleFunction("Exit"));
		return false;

	if (!m_pMainConsole->UnregisterConsoleFunction("SetRasterModeWireframe"));
		return false;

	if (!m_pMainConsole->UnregisterConsoleFunction("SetRasterModeWireframeNoCull"));
		return false;

	if (!m_pMainConsole->UnregisterConsoleFunction("SetRasterModeDefault"));
		return false;

	if (!m_pMainConsole->UnregisterConsoleFunction("ResetRasterState"));
		return false;

	return true;
}

void CConsoleFunctions::Exit()
{
	PostMessage(CConsoleFunctions::GetInstance()->m_hMainWindow, WM_QUIT, 0, 0);

	return;
}

void CConsoleFunctions::ResetRasterState()
{
	CConsoleFunctions::GetInstance()->m_pRenderer->GetDirect3D()->ResetRasterState();

	return;
}

void CConsoleFunctions::SetRasterModeWireframe()
{
	CConsoleFunctions::GetInstance()->m_pRenderer->GetDirect3D()->SetRasterState(CConsoleFunctions::GetInstance()->m_pRenderer->GetDirect3D()->GetRSWireFrameCullBack());

	return;
}

void CConsoleFunctions::SetRasterModeWireframeNoCull()
{
	CConsoleFunctions::GetInstance()->m_pRenderer->GetDirect3D()->SetRasterState(CConsoleFunctions::GetInstance()->m_pRenderer->GetDirect3D()->GetRSWireFrameNoCull());

	return;
}

void CConsoleFunctions::SetRasterModeDefault()
{
	CConsoleFunctions::GetInstance()->m_pRenderer->GetDirect3D()->SetRasterState(CConsoleFunctions::GetInstance()->m_pRenderer->GetDirect3D()->GetRSDefault());

	return;
}

CConsoleFunctions * CConsoleFunctions::GetInstance()
{
	return m_pInstance;
}

CConsoleFunctions * CConsoleFunctions::GetInstance(HWND hWnd, CConsole* pConsole, CRenderer* pRenderer)
{
	if (!m_pInstance)
		m_pInstance = new CConsoleFunctions(hWnd, pConsole, pRenderer);

	return m_pInstance;
}