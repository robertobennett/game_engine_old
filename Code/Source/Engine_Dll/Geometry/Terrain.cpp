#include <Include\Engine_Dll\stdafx.h>
#include "..\..\..\Include\Engine_Dll\Geometry\Terrain.h"

CTerrain::CTerrain():
m_bCreated(false),
m_iMaxHeight(0),
m_iWidth(0),
m_iDepth(0),
m_pRenderer(nullptr),
m_pFileManager(nullptr)
{
	return;
}

CTerrain::CTerrain(std::string sName, std::string sPath, CRenderer * pRend, IShader * pShader):
m_bCreated(false),
m_sTerrainName(sName),
m_sTerrainFolderPath(sPath),
m_iMaxHeight(0),
m_iWidth(0),
m_iDepth(0),
m_pRenderer(pRend),
m_pFileManager(nullptr)
{
	m_pBoundShader = pShader;
	return;
}

CTerrain::~CTerrain()
{
	return;
}

bool CTerrain::Create()
{
	if (!m_bCreated)
	{
		std::stringstream ssTerrainSettingsPath;
		ssTerrainSettingsPath << m_sTerrainFolderPath << m_sTerrainName << ".settings\0";

		m_pFileManager = new CFileManager();
		m_pFileManager->Create(ssTerrainSettingsPath.str());

		if (!LoadTerrainSettings())
			return false;
	
		if (!LoadTerrainBitmap())
			return false;

		if (!CreateGeometry())
			return false;

		XMStoreFloat4x4(&m_xmf4x4WorldMatrix, XMMatrixIdentity() * XMMatrixTranslationFromVector(XMLoadFloat4(&XMFLOAT4((-m_iWidth / 2), 0, (-m_iDepth / 2), 1))));

		return true;
	}

	return true;
}

inline bool CTerrain::LoadTerrainSettings()
{
	m_pFileManager->ReadFile();
	m_Data = m_pFileManager->GetData();

	//Terrain file should be in following format
	/*
	iMaxHeight=
	iMinHeight=
	iWidth=
	iDepth=
	*/

	m_iMaxHeight = atoi(m_Data[0]->sValue.c_str());
	m_iWidth = atoi(m_Data[1]->sValue.c_str());
	m_iDepth = atoi(m_Data[2]->sValue.c_str());

	return true;
}

bool CTerrain::LoadTerrainBitmap()
{
	/*
	Useful resources:
	http://www.tutorialspoint.com/cprogramming/c_file_io.htm - C standard file I/O stream
	https://en.wikipedia.org/wiki/BMP_file_format#/media/File:BMPfileFormat.png - Image showing bitmap file structure
	https://msdn.microsoft.com/en-us/library/windows/desktop/dd183374(v=vs.85).aspx - MSDN doc on BITMAPFILEHEADER sturcture
	https://msdn.microsoft.com/en-us/library/windows/desktop/dd183376(v=vs.85).aspx - MSDN doc on BITMAPINFOHEADER structure
	*/

	//Create a string stream that will hold all the information read and calculated with relation to the bitmap
	std::stringstream ssDebugFileReport;

	//Create a file path to the terrain bitmap
	std::stringstream ssTerrainBmpPath;
	ssTerrainBmpPath << m_sTerrainFolderPath << m_sTerrainName << ".bmp\0";

	//Create a pointer to the file, use C file system rather standard library
	FILE* pBmpFile = nullptr;
	//Open the file in read binary (rb) mode, check it opened by checking if it returned zero
	if (fopen_s(&pBmpFile, ssTerrainBmpPath.str().c_str(), "rb") != 0 && pBmpFile != nullptr)
	{
		std::stringstream ssErrOut;
		ssErrOut << "Failed to load bitmap file " << ssTerrainBmpPath.str().c_str() << "either file not found or corrupted !";
		MessageBoxA(NULL, ssErrOut.str().c_str(), "FAILED: To load bitmap file !", MB_OK);
		return false;
	}

	//Read bitmap header and info header
	//bitmapfileheader https://msdn.microsoft.com/en-us/library/windows/desktop/dd183374(v=vs.85).aspx
	//bitmapheaderinfo https://msdn.microsoft.com/en-us/library/windows/desktop/dd183376(v=vs.85).aspx
	BITMAPFILEHEADER bmpFileHeader; ZeroMemory(&bmpFileHeader, sizeof(BITMAPFILEHEADER));
	//Attempt to read the bitmapfileheader, as we are only reading 1 item, check that fread returns 1 if it doesnt it has failed to read
	if (fread(&bmpFileHeader, sizeof(BITMAPFILEHEADER), 1, pBmpFile) != 1)
		return false;

	BITMAPINFOHEADER bmpInfoHeader; ZeroMemory(&bmpInfoHeader, sizeof(BITMAPINFOHEADER));
	//Attempt to read the bitmapinfoheader, as we are only reading 1 item, check that fread returns 1 if it doesnt it has failed to read
	if (fread(&bmpInfoHeader, sizeof(BITMAPINFOHEADER), 1, pBmpFile) != 1)
		return false;

#ifdef _DEBUG
	ssDebugFileReport
		<< "---------------------------------\nbmpFileHeader.bfOffBits: "
		<< bmpFileHeader.bfOffBits
		<< "\nbmpFileHeader.bfReserved1: "
		<< bmpFileHeader.bfReserved1
		<< "\nbmpFileHeader.bfReserved2: "
		<< bmpFileHeader.bfReserved2
		<< "\nbmpFileHeader.bfSize: "
		<< bmpFileHeader.bfSize
		<< "\nbmpFileHeader.bfType: "
		<< bmpFileHeader.bfType
		<< "\n\nbmpInfoHeader.biBitCount: "
		<< bmpInfoHeader.biBitCount
		<< "\nbmpInfoHeader.biClrImportant: "
		<< bmpInfoHeader.biClrImportant
		<< "\nbmpInfoHeader.biClrUsed: "
		<< bmpInfoHeader.biClrUsed
		<< "\nbmpInfoHeader.biCompression: "
		<< bmpInfoHeader.biCompression
		<< "\nbmpInfoHeader.biHeight: "
		<< bmpInfoHeader.biHeight
		<< "\nbmpInfoHeader.biPlanes: "
		<< bmpInfoHeader.biPlanes
		<< "\nbmpInfoHeader.biSize: "
		<< bmpInfoHeader.biSize
		<< "\nbmpInfoHeader.biSizeImage: "
		<< bmpInfoHeader.biSizeImage
		<< "\nbmpInfoHeader.biWidth: "
		<< bmpInfoHeader.biWidth
		<< "\nbmpInfoHeader.biXPelsPerMeter: "
		<< bmpInfoHeader.biXPelsPerMeter
		<< "\nbmpInfoHeader.biYPelsPerMeter: "
		<< bmpInfoHeader.biYPelsPerMeter
		<< "\n";
#endif

	/*
		We can use biBitCount to find out how many colour channels the bit map is using this will let us calculate the offset for each individual pixel as we are reading from monochromatic bmp
		the colour value for each channel is the same, we are going to simply devide biBitCount by 8 to calculate the offset, we can discard any value lower than 8 and say it is not a valid format as
		we want to map from 0-255 which 4 bits will not hold as 2^4 = 16, below is a discription of each value

		biBitCount
			The number of bits-per-pixel. The biBitCount member of the BITMAPINFOHEADER structure determines the number of
			bits that define each pixel and the maximum number of colors in the bitmap. This member must be one of the following values.
			Value meaning

		0	The number of bits-per-pixel is specified or is implied by the JPEG or PNG format.

		1	The bitmap is monochrome, and the bmiColors member of BITMAPINFO contains two entries.
		Each bit in the bitmap array represents a pixel. If the bit is clear, the pixel is displayed with
		the color of the first entry in the bmiColors table; if the bit is set, the pixel has the color of the second entry in the table.

		4	The bitmap has a maximum of 16 colors, and the bmiColors member of BITMAPINFO contains up to 16 entries.
		Each pixel in the bitmap is represented by a 4-bit index into the color table. For example, if the first byte in the
		bitmap is 0x1F, the byte represents two pixels. The first pixel contains the color in the second table entry, and the
		second pixel contains the color in the sixteenth table entry.

		8	The bitmap has a maximum of 256 colors, and the bmiColors member of BITMAPINFO contains up to 256 entries. In this case,
		each byte in the array represents a single pixel.

		16	The bitmap has a maximum of 2^16 colors. If the biCompression member of the BITMAPINFOHEADER is BI_RGB, the bmiColors member of BITMAPINFO is NULL.
		Each WORD in the bitmap array represents a single pixel. The relative intensities of red, green, and blue are represented with five bits for each color component.
		The value for blue is in the least significant five bits, followed by five bits each for green and red. The most significant bit is not used. The bmiColors color table
		is used for optimizing colors used on palette-based devices, and must contain the number of entries specified by the biClrUsed member of the BITMAPINFOHEADER.
		If the biCompression member of the BITMAPINFOHEADER is BI_BITFIELDS, the bmiColors member contains three DWORD color masks that specify the red, green, and blue components,
		respectively, of each pixel. Each WORD in the bitmap array represents a single pixel.
		When the biCompression member is BI_BITFIELDS, bits set in each DWORD mask must be contiguous and should not overlap the bits of another mask. All the bits in the pixel do not have to be used.

		24	The bitmap has a maximum of 2^24 colors, and the bmiColors member of BITMAPINFO is NULL. Each 3-byte triplet in the bitmap array represents the relative intensities of blue, green,
		and red, respectively, for a pixel. The bmiColors color table is used for optimizing colors used on palette-based devices, and must contain the number of entries specified by the biClrUsed
		member of the BITMAPINFOHEADER.

		32	The bitmap has a maximum of 2^32 colors. If the biCompression member of the BITMAPINFOHEADER is BI_RGB, the bmiColors member of BITMAPINFO is NULL. Each DWORD in the
		bitmap array represents the relative intensities of blue, green, and red for a pixel. The value for blue is in the least significant 8 bits, followed by 8 bits each for green
		and red. The high byte in each DWORD is not used. The bmiColors color table is used for optimizing colors used on palette-based devices, and must contain the number of entries
		specified by the biClrUsed member of the BITMAPINFOHEADER.
		If the biCompression member of the BITMAPINFOHEADER is BI_BITFIELDS, the bmiColors member contains three DWORD color masks that specify the red, green, and blue components, respectively,
		of each pixel. Each DWORD in the bitmap array represents a single pixel.
		When the biCompression member is BI_BITFIELDS, bits set in each DWORD mask must be contiguous and should not overlap the bits of another mask. All the bits in the pixel do not need to be used
	*/

	if (bmpInfoHeader.biBitCount < 8)
	{
		std::stringstream ssErrOut;
		ssErrOut << "Failed to load terrain " << ssTerrainBmpPath.str().c_str() << " as bmpInfoHeader.biBitCount is less than 8 ! Please load a bitmap with a a minimum of a 8 bit colour channel.";
		MessageBoxA(NULL, ssErrOut.str().c_str(), "FAILED: To load terrain !", MB_OK);

#ifdef _DEBUG
		OutputDebugStringA(ssDebugFileReport.str().c_str());
#endif

		return false;
	}

	int iBmpColourChannelCount = bmpInfoHeader.biBitCount / 8;

#ifdef _DEBUG
	ssDebugFileReport << "\nFile uses "<< iBmpColourChannelCount << " * 8 bit colour channels !\n---------------------------------\n";
#endif

	//Check if the width and height of the bmp are the same as the width and depth of the expected terrain bmp (as read from the .settings file)
	if (bmpInfoHeader.biWidth != m_iWidth && bmpInfoHeader.biHeight != m_iDepth)
	{
		std::stringstream ssErrOut;
		ssErrOut << "Failed to load terrain " << ssTerrainBmpPath.str().c_str() << " as the bitmap width and height do not match the width and depth defined in the terrain .settings file !";
		MessageBoxA(NULL, ssErrOut.str().c_str(), "FAILED: To load terrain !", MB_OK);

#ifdef _DEBUG
		OutputDebugStringA(ssDebugFileReport.str().c_str());
#endif

		return false;
	}

	//Seek to the end of the info header, as the bitmap has to have 8bit colour there is no colour table, the end of the info header is the pixel array so we read the pixel info from here
	//https://upload.wikimedia.org/wikipedia/commons/c/c4/BMPfileFormat.png - shows the structure of a bitmap
	//bfOffBits - The offset, in bytes, from the beginning of the BITMAPFILEHEADER structure to the bitmap bits. (https://msdn.microsoft.com/en-us/library/windows/desktop/dd183374(v=vs.85).aspx)
	//gives the offset from start of file to the "bitmap bits" which is the pixel array
	//http://www.cplusplus.com/reference/cstdio/fseek/ - fseek documentation

	//Seek to the pixel array by using bfOffBits from the start of SEEK_SET
	if (fseek(pBmpFile, bmpFileHeader.bfOffBits, SEEK_SET) != 0)
	{
		std::stringstream ssErrOut;
		ssErrOut << "Failed to load terrain " << ssTerrainBmpPath.str().c_str() << " fseek(...) failed to seek to the start of pixel array !";
		MessageBoxA(NULL, ssErrOut.str().c_str(), "FAILED: To load terrain !", MB_OK);

#ifdef _DEBUG
		OutputDebugStringA(ssDebugFileReport.str().c_str());
#endif

		return false;
	}

	//Check if the bmp file has padding - https://upload.wikimedia.org/wikipedia/commons/c/c4/BMPfileFormat.png - shows structure of bmp file
	//Bmp files have padding to make the pixel row a multiple of four bytes, so if bmpInfoHeader.biBitCount == 24 we need to handle the reading differently
	if (bmpInfoHeader.biBitCount == 0)
	{
		std::stringstream ssErrOut;
		ssErrOut << "Failed to load terrain " << ssTerrainBmpPath.str().c_str() << " bmpInfoHeader.biBitCount == 24 currently not supported !";
		MessageBoxA(NULL, ssErrOut.str().c_str(), "FAILED: To load terrain !", MB_OK);

#ifdef _DEBUG
		OutputDebugStringA(ssDebugFileReport.str().c_str());
#endif

		return false;
	}
	else
	{
		//Create a buffer to hold the data
		unsigned int iExpectedElementCount = m_iWidth * m_iDepth;
		unsigned int iPixelSize = iBmpColourChannelCount;
		unsigned char * pBmpBuff = new unsigned char[m_iWidth * m_iDepth * iBmpColourChannelCount];

		if (fread(pBmpBuff, sizeof(char) * iBmpColourChannelCount, (m_iWidth * m_iDepth), pBmpFile) != (m_iWidth * m_iDepth))
		{
			std::stringstream ssErrOut;
			ssErrOut << "Failed to load terrain " << ssTerrainBmpPath.str().c_str() << " items read from file was less than expected !";
			MessageBoxA(NULL, ssErrOut.str().c_str(), "FAILED: To load terrain !", MB_OK);

#ifdef _DEBUG
			OutputDebugStringA(ssDebugFileReport.str().c_str());
#endif
			delete[] pBmpBuff;
			pBmpBuff = nullptr;

			return false;
		}

		//Resize the heightmap vector to hold enoguh points width * depth
		m_HeightMapDesc.resize((m_iDepth * m_iWidth));
		//Iterate through every pixel in bmp file by iterating through the width and height allowing us to fill out the height map desc at the same time
		unsigned int iPixelIndex = 0;
		for (unsigned int d = 0; d < m_iDepth; d++)
		{
			for (unsigned int w = 0; w < m_iWidth; w++)
			{
				//The index for the point on the height map is equal to the row number times by the depth plus the column number, read the data in backwards
				unsigned int iPointIndex = ((d * m_iDepth) + w);

				m_HeightMapDesc[iPointIndex].x = w;
				m_HeightMapDesc[iPointIndex].y = MapHeight((float)(pBmpBuff[iPixelIndex]));
				m_HeightMapDesc[iPointIndex].z = d;

				iPixelIndex += iBmpColourChannelCount;
			}
		}

#ifdef _DEBUG
		std::ofstream ofBmpDataDmp;
		std::ofstream ofHMDataDmp;

		std::stringstream ssBmpDmpFilePath, ssHMDmpFilePath;
		ssBmpDmpFilePath << m_sTerrainFolderPath << "pBmpBuff.dump";
		ssHMDmpFilePath << m_sTerrainFolderPath << "HeightMap.dump";
		ofBmpDataDmp.open(ssBmpDmpFilePath.str().c_str(), std::ofstream::out);
		ofHMDataDmp.open(ssHMDmpFilePath.str().c_str(), std::ofstream::out);

		if (ofBmpDataDmp.is_open() && ofHMDataDmp.is_open())
		{
			int d = 1;
			for (unsigned int i = 0; i < (m_iWidth * m_iDepth); i++)
			{
				ofBmpDataDmp << (float)pBmpBuff[i] << " ";
				ofHMDataDmp << "(" << m_HeightMapDesc[i].x << ", " << m_HeightMapDesc[i].y << ", " << m_HeightMapDesc[i].z << ") ";

				if (i > 1 && ((i + 1) - (d * m_iWidth)) == 0)
				{
					ofHMDataDmp << "\n";
					ofBmpDataDmp << "\n";
					d++;
				}
			}
		}

		if (ofBmpDataDmp.is_open())
			ofBmpDataDmp.close();

		if (ofHMDataDmp.is_open())
			ofHMDataDmp.close();
#endif

		delete[] pBmpBuff;
		pBmpBuff = nullptr;
	}

	fclose(pBmpFile);
	pBmpFile = nullptr;


#ifdef _DEBUG
	OutputDebugStringA(ssDebugFileReport.str().c_str());
#endif

	return true;
}

inline bool CTerrain::CreateGeometry()
{
	m_pD3D11RS = m_pRenderer->GetDirect3D()->GetRSDefault();

	//fill out vertex structure and push it into the vertex vector
	for (UINT i = 0; i < m_HeightMapDesc.size(); i++)
	{
		GlobalDefs::SimpleVertex sv; ZeroMemory(&sv, sizeof(GlobalDefs::SimpleVertex));
		sv.xmf3Pos.x = m_HeightMapDesc[i].x;
		sv.xmf3Pos.y = m_HeightMapDesc[i].y;
		sv.xmf3Pos.z = m_HeightMapDesc[i].z;

		sv.xmf4Colour = XMFLOAT4(.29803, .6, 0, 1);

		sv.xmf3Norm = XMFLOAT3(0, 0, 0);

		m_Verts.push_back(sv);
	}

	//Create a temorary container big enough to hold a normal vector for each face in the mesh
	std::vector<XMVECTOR> xmvFaceNormals;
	
	//iterate through width - 1 verts and depth - 1 verts as we want to calculate it for each face which will be the bottom right face for each vert except the end ones which wont have a bottom right face
	for (unsigned int d = 0; d < (m_iDepth - 1); d++)
	{
		for (unsigned int w = 0; w < (m_iWidth - 1); w++)
		{
			unsigned int iVertAIndex, iVertBIndex, iVertCIndex;
			iVertAIndex = (((d)* m_iDepth) + (w));
			iVertBIndex = (((d)* m_iDepth) + (w + 1));
			iVertCIndex = (((d + 1)* m_iDepth) + (w));

			XMVECTOR xmvFaceNormal = XMVectorZero();
			//Use our function defined in IGeometry that takes three verts A, B, C where A equals the iterated vertex, B equals the vertex to the right of A, C equals the vertex below A
			CalculateFaceNormal(&m_Verts[iVertAIndex].xmf3Pos, &m_Verts[iVertBIndex].xmf3Pos, &m_Verts[iVertCIndex].xmf3Pos, xmvFaceNormal);

			//Add this face normal to the temp container
			xmvFaceNormals.push_back(xmvFaceNormal);
		}
	}

	//Now for each vertex we need to take the average face normal vector from the normals of all the faces sorrounding each vertex
	for (unsigned int d = 0; d < m_iDepth; d++)
	{
		for (unsigned int w = 0; w < m_iWidth; w++)
		{
			unsigned int iVertexIndex, iTopLeftFaceIndex, iTopRightFaceIndex, iBottomLeftFaceIndex, iBottomRightFaceIndex;
			iVertexIndex = (((d) * (m_iDepth)) + (w));
			iTopLeftFaceIndex = (((d - 1)* (m_iDepth - 1)) + (w - 1));
			iTopRightFaceIndex = (((d - 1)* (m_iDepth - 1)) + (w));
			iBottomLeftFaceIndex = (((d)* (m_iDepth - 1)) + (w - 1));
			iBottomRightFaceIndex = (((d)* (m_iDepth - 1)) + (w));

			XMVECTOR xmvSumOfFaceNormals = XMVectorZero();

			//top left vertex
			if (d == 0 && w == 0)
			{
				//No need to average it is just the bottom right face
				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvFaceNormals[iBottomRightFaceIndex]);
			}
			//top right vertex
			else if (d == 0 && w == (m_iWidth - 1))
			{
				//No need to average it is just the bottom left face
				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvFaceNormals[iBottomLeftFaceIndex]);
			}
			//bottom left vertex
			else if (d == (m_iDepth - 1) && w == 0)
			{
				//No need to average it is just the top right face
				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvFaceNormals[iTopRightFaceIndex]);
			}
			//bottom right vertex
			else if (d == (m_iDepth - 1) && w == (m_iWidth - 1))
			{
				//No need to average it is just the top left face
				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvFaceNormals[iTopLeftFaceIndex]);
			}
			//top edge vertex
			else if (d == 0)
			{
				//Average the bottom left and bottom right faces
				xmvSumOfFaceNormals += xmvFaceNormals[iBottomLeftFaceIndex];
				xmvSumOfFaceNormals += xmvFaceNormals[iBottomRightFaceIndex];
				xmvSumOfFaceNormals /= 2;

				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvSumOfFaceNormals);
			}
			//bottom edge vertex
			else if (d == (m_iDepth - 1))
			{
				//Average the top left and top right faces
				xmvSumOfFaceNormals += xmvFaceNormals[iTopLeftFaceIndex];
				xmvSumOfFaceNormals += xmvFaceNormals[iTopRightFaceIndex];
				xmvSumOfFaceNormals /= 2;

				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvSumOfFaceNormals);
			}
			//left edge vertex
			else if (w == 0)
			{
				//Average the top right and bottom right faces
				xmvSumOfFaceNormals += xmvFaceNormals[iBottomRightFaceIndex];
				xmvSumOfFaceNormals += xmvFaceNormals[iTopRightFaceIndex];
				xmvSumOfFaceNormals /= 2;

				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvSumOfFaceNormals);
			}
			//right edge vertex
			else if (w == (m_iWidth - 1))
			{
				//Average the top left and bottom left faces
				xmvSumOfFaceNormals += xmvFaceNormals[iTopLeftFaceIndex];
				xmvSumOfFaceNormals += xmvFaceNormals[iBottomLeftFaceIndex];
				xmvSumOfFaceNormals /= 2;

				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvSumOfFaceNormals);
			}
			//normal vertex
			else
			{

				//Average all four faces
				xmvSumOfFaceNormals += xmvFaceNormals[iTopLeftFaceIndex];
				xmvSumOfFaceNormals += xmvFaceNormals[iTopRightFaceIndex];
				xmvSumOfFaceNormals += xmvFaceNormals[iBottomLeftFaceIndex];
				xmvSumOfFaceNormals += xmvFaceNormals[iBottomRightFaceIndex];
				xmvSumOfFaceNormals /= 4;

				XMStoreFloat3(&m_Verts[iVertexIndex].xmf3Norm, xmvSumOfFaceNormals);
			}
		}
	}

	//Create indeice ordering ect.
	for (unsigned int d = 0; d < (m_iDepth - 1); d++)
	{
		for (unsigned int w = 0; w < (m_iWidth - 1); w++)
		{
			//Store the vertex numbers for this quad
			unsigned int iV1, iV2, iV3, iV4;
			iV1 = (((d + 1) * m_iDepth) + (w)); //w0 d1 // bottom left
			iV2 = (((d)* m_iDepth) + (w)); //w0 d0 - top left
			iV3 = (((d)* m_iDepth) + (w + 1)); //w1 d0 - top right
			iV4 = (((d + 1)* m_iDepth) + (w + 1));//w1 d1 - bottom right

			//Clock wise winding doesnt work :/ ?????
			m_Indicies.push_back(iV1);
			m_Indicies.push_back(iV3);
			m_Indicies.push_back(iV2);

			m_Indicies.push_back(iV1);
			m_Indicies.push_back(iV4);
			m_Indicies.push_back(iV3);
		}
	}

	HRESULT hRes = S_OK;

	//Create vertex buffer description
	D3D11_BUFFER_DESC vbd; ZeroMemory(&vbd, sizeof(D3D11_BUFFER_DESC));
	vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	vbd.ByteWidth = sizeof(GlobalDefs::SimpleVertex) * m_Verts.size();
	vbd.Usage = D3D11_USAGE_DEFAULT;

	//Create subreasource data
	D3D11_SUBRESOURCE_DATA vid; ZeroMemory(&vid, sizeof(D3D11_SUBRESOURCE_DATA));
	vid.pSysMem = &(m_Verts[0]);

	//Attempt to create vertex buffer
	hRes = m_pRenderer->GetDirect3D()->D3D11CreateBuffer(&vbd, &vid, &m_pD3D11VertexBuffer);
	if (FAILED(hRes))
	{
		return false;
	}

	D3D11_BUFFER_DESC ibd; ZeroMemory(&ibd, sizeof(D3D11_BUFFER_DESC));
	ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
	ibd.ByteWidth = sizeof(UINT) * m_Indicies.size();
	ibd.Usage = D3D11_USAGE_DEFAULT;

	D3D11_SUBRESOURCE_DATA iid; ZeroMemory(&iid, sizeof(D3D11_SUBRESOURCE_DATA));
	iid.pSysMem = &(m_Indicies[0]);

	hRes = m_pRenderer->GetDirect3D()->D3D11CreateBuffer(&ibd, &iid, &m_pD3D11IndexBuffer);
	if (FAILED(hRes))
		return false;


	m_iVertexCount = m_Verts.size();
	m_iIndicieCount = m_Indicies.size();
	m_iVertexBufferStride = sizeof(GlobalDefs::SimpleVertex);

	return IGeometry::Create();
}

float CTerrain::MapHeight(float fIn)
{
	//255 = m_fMaxHeight
	//0 = 0

	float fOut = 0.0f;
	fOut = fIn / 255;
	fOut *= m_iMaxHeight;

	return fOut;
}

void CTerrain::Destroy()
{
	return IGeometry::Destroy();
}