#include <Include\Engine_Dll\stdafx.h>
#include <Include\Engine_Dll\Geometry\Primitives\Box.h>

CBox::CBox(CRenderer * pRenderer, IShader * pShader, XMFLOAT4 pos):
m_bCreated(false)
{
	m_xmf4Pos = pos;
	m_pRenderer = pRenderer;
	m_pBoundShader = pShader;

	return;
}

CBox::~CBox()
{
	return;
}

bool CBox::Create()
{
	if (!m_bCreated)
	{
		HRESULT hRes = S_OK;
		m_pD3D11RS = m_pRenderer->GetDirect3D()->GetRSDefault();

		if (m_pRenderer == nullptr)
			return false;
		
		XMStoreFloat4x4(&m_xmf4x4WorldMatrix, XMMatrixIdentity());
		XMStoreFloat4x4(&m_xmf4x4WorldMatrix, XMMatrixTranslationFromVector(XMLoadFloat4(&m_xmf4Pos)));

		GlobalDefs::SimpleVertex verts[] =
		{
			{ XMFLOAT3(-1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(-1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(1.0f, 1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(1.0f, -1.0f, -1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(-1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(-1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f) },
			{ XMFLOAT3(1.0f, -1.0f, 1.0f), XMFLOAT3(0.0f, 0.0f, 0.0f) }
		};

		D3D11_BUFFER_DESC vbd; ZeroMemory(&vbd, sizeof(D3D11_BUFFER_DESC));
		vbd.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		vbd.ByteWidth = sizeof(GlobalDefs::SimpleVertex) * 8;
		vbd.CPUAccessFlags = 0;
		vbd.Usage = D3D11_USAGE_DEFAULT;
		vbd.StructureByteStride = 0;
		vbd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA vid; ZeroMemory(&vid, sizeof(D3D11_SUBRESOURCE_DATA));
		vid.pSysMem = verts;

		hRes = m_pRenderer->GetDirect3D()->D3D11CreateBuffer(&vbd, &vid, &m_pD3D11VertexBuffer);
		if (FAILED(hRes))
		{
			return false;
		}

		UINT indicies[] =
		{
			0, 2, 3,
			0, 3, 1,
			4, 6, 2,
			4, 2, 0,
			1, 3, 7,
			1, 7, 5,
			2, 6, 7,
			2, 7, 3,
			6, 4, 5,
			6, 5, 7,
			4, 0, 1,
			4, 1, 5
		};

		D3D11_BUFFER_DESC ibd; ZeroMemory(&ibd, sizeof(D3D11_BUFFER_DESC));
		ibd.BindFlags = D3D11_BIND_INDEX_BUFFER;
		ibd.ByteWidth = sizeof(UINT) * 36;
		ibd.CPUAccessFlags = 0;
		ibd.Usage = D3D11_USAGE_DEFAULT;
		ibd.StructureByteStride = 0;
		ibd.MiscFlags = 0;

		D3D11_SUBRESOURCE_DATA iid; ZeroMemory(&vid, sizeof(D3D11_SUBRESOURCE_DATA));
		iid.pSysMem = indicies;

		hRes = m_pRenderer->GetDirect3D()->D3D11CreateBuffer(&ibd, &iid, &m_pD3D11IndexBuffer);
		if (FAILED(hRes))
		{
			return false;
		}

		m_iIndicieCount = 36;
		m_iVertexCount = 8;
		m_iVertexBufferStride = sizeof(GlobalDefs::SimpleVertex);

		m_bCreated = true;
		return IGeometry::Create();
	}

	return IGeometry::Create();
}

void CBox::Update()
{
	return IGeometry::Update();
}

void CBox::Draw()
{
	return IGeometry::Draw();
}

void CBox::Destroy()
{
	return IGeometry::Destroy();
}