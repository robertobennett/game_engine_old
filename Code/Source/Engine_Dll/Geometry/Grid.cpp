#include <Include\Engine_Dll\stdafx.h>
#include <Include\Engine_Dll\Geometry\Grid.h>

CGrid::CGrid(CRenderer * pRenderer, IShader * pShader, XMFLOAT4 pos, int iWidth, int iDepth, bool bVertGrid) :
m_bCreated(false),
m_bVerticalGrid(bVertGrid)
{
	m_pRenderer = pRenderer;
	m_pBoundShader = pShader;
	m_xmf4Pos = pos;
	m_iDepth = iDepth;
	m_iWidth = iWidth;
	XMStoreFloat4x4(&m_xmf4x4WorldMatrix, XMMatrixIdentity() * XMMatrixTranslationFromVector(XMLoadFloat4(&pos)));

	return;
}

CGrid::~CGrid()
{
	return;
}

bool CGrid::Create()
{
	if (!m_bCreated)
	{
		HRESULT hRes = S_OK;
		int iXStart = (m_iWidth / -2);
		int iDepthStart = (m_iDepth / -2);
		int iStep = 1;
		
		m_pD3D11RS = m_pRenderer->GetDirect3D()->GetRSWireFrameNoCull();

		if (m_bVerticalGrid)
		{
			for (int y = iDepthStart; (y - 1) < (iDepthStart + m_iDepth) / iStep; y += iStep)
			{
				for (int x = iXStart; (x - 1) < (iXStart + m_iWidth) / iStep; x += iStep)
				{
					m_Verts.push_back({ XMFLOAT3(x, y, 0.0f), XMFLOAT3(1.0f, 1.0f, 1.0f) });
				}
			}
		}
		else
		{
			for (int z = iDepthStart; (z - 1) < (iDepthStart + m_iDepth) / iStep; z += iStep)
			{
				for (int x = iXStart; (x - 1) < (iXStart + m_iWidth) / iStep; x += iStep)
				{
					m_Verts.push_back({ XMFLOAT3(x, 0.0f, z), XMFLOAT3(1.0f, 1.0f, 1.0f) });
				}
			}
		}

		m_iVertexCount = m_Verts.size();
		m_iVertexBufferStride = sizeof(GlobalDefs::SimpleVertex);

		D3D11_BUFFER_DESC gridVertBufferDesc; ZeroMemory(&gridVertBufferDesc, sizeof(D3D11_BUFFER_DESC));
		gridVertBufferDesc.BindFlags = D3D11_BIND_VERTEX_BUFFER;
		gridVertBufferDesc.ByteWidth = sizeof(GlobalDefs::SimpleVertex) * m_Verts.size();
		gridVertBufferDesc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA initVertData; ZeroMemory(&initVertData, sizeof(D3D11_SUBRESOURCE_DATA));
		initVertData.pSysMem = &(m_Verts[0]);

		hRes = m_pRenderer->GetDirect3D()->D3D11CreateBuffer(&gridVertBufferDesc, &initVertData, &m_pD3D11VertexBuffer);
		if (FAILED(hRes))
			return false;

		//For each quad
		//Tri 1 = (i, i + iWidth + 1, i + iWidth)
		//Tir 2 = (i, i + 1, i + 1 + iWidth)
		/*
		m_iWidth = 2
		m_iHeight = 2
		   |0| 1 |2|
		|0|[0][1][2]
		|1|[3][4][5]
		|2|[6][7][8]

		0, 4, 3, | x, x + m_iVertPerRow + 1, x + m_iVertPerRow
		0, 1, 4, | x, x + 1, x + m_iVertPerRow + 1

		1, 5, 4, | 
		1, 2, 5,

		3, 7, 6,
		3, 4, 7,

		4, 8, 7,
		4, 5, 8,
		*/
		//m = the number of verts in a row
		int iNumVertsInHorizontal = m_iWidth + 1;
		//n = the number of verts in a column
		int iNumVertsInVertical = m_iDepth + 1;
		//For every vertex before one before the last vertex, so n verts we stop before n-1 verts, this stops the algorithm from breaking as it is like drawing another triangle onto the last vertex
		for (UINT xVertNum = 0; xVertNum < (iNumVertsInHorizontal - 1); xVertNum++)
		{
			//For every vertex in the vertical for that row
			for (UINT yVertNum = 0; yVertNum < (iNumVertsInVertical - 1); yVertNum++)
			{
				//Create one triangle
				m_Inicies.push_back(xVertNum * iNumVertsInVertical + yVertNum);
				m_Inicies.push_back((xVertNum + 1) * iNumVertsInVertical + yVertNum);
				m_Inicies.push_back(xVertNum * iNumVertsInVertical + yVertNum +1);

				//Create the other triangle to make up the quad
				m_Inicies.push_back((xVertNum + 1) * iNumVertsInVertical + yVertNum);
				m_Inicies.push_back((xVertNum + 1) * iNumVertsInVertical + yVertNum + 1);
				m_Inicies.push_back(xVertNum * iNumVertsInVertical + yVertNum + 1);
			}
		}

		D3D11_BUFFER_DESC gridIndexBufferDesc; ZeroMemory(&gridIndexBufferDesc, sizeof(D3D11_BUFFER_DESC));
		gridIndexBufferDesc.BindFlags = D3D11_BIND_INDEX_BUFFER;
		gridIndexBufferDesc.ByteWidth = sizeof(UINT) * m_Inicies.size();
		gridIndexBufferDesc.Usage = D3D11_USAGE_DEFAULT;

		D3D11_SUBRESOURCE_DATA initIndData; ZeroMemory(&initIndData, sizeof(D3D11_SUBRESOURCE_DATA));
		initIndData.pSysMem = &(m_Inicies[0]);

		m_iIndicieCount = m_Inicies.size();

		hRes = m_pRenderer->GetDirect3D()->D3D11CreateBuffer(&gridIndexBufferDesc, &initIndData, &m_pD3D11IndexBuffer);
		if (FAILED(hRes))
			return false;

		m_bCreated = true;
		return IGeometry::Create();
	}

	return IGeometry::Create();
}

void CGrid::Update()
{
}

void CGrid::Draw()
{
}

void CGrid::Destroy()
{
}