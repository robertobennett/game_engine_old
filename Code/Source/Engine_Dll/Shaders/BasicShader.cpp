#include <Include\Engine_Dll\stdafx.h>
#include <Include\Engine_Dll\Shaders\BasicShader.h>

CBasicShader::CBasicShader(CRenderer * pRenderer):
m_bCreated(false)
{
	m_pRenderer = pRenderer;

	return;
}

CBasicShader::~CBasicShader()
{
	return;
}

bool CBasicShader::Create()
{
	if (!m_bCreated && m_pRenderer != nullptr)
	{
		HRESULT hRes = S_OK;
		ID3D10Blob * pVertexBlob = nullptr;
		ID3D10Blob * pErrorBlob = nullptr;

		m_wssShaderPath << wGetRootFolderPath() << "Shaders\\SimpleShader.hlsl\0";

		m_D3D11PrimitiveTopology = D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

		hRes = D3DCompileFromFile(m_wssShaderPath.str().c_str(), NULL, NULL, "vsMain", "vs_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &pVertexBlob, &pErrorBlob);
		if (FAILED(hRes))
		{
			char* pCompErrors = nullptr;
			UINT iBuffSize = 0;
			std::stringstream ssOut;
			// Get a pointer to the error message text buffer.
			pCompErrors = (char*)(pErrorBlob->GetBufferPointer());

			// Get the length of the message.
			iBuffSize = pErrorBlob->GetBufferSize();

			// Write out the error message.
			for (UINT i = 0; i< iBuffSize; i++)
			{
				ssOut << pCompErrors[i];
			}

			OutputDebugStringA(ssOut.str().c_str());

			// Release the error message.
			pErrorBlob->Release();
			pErrorBlob = nullptr;

			return false;
		}

		hRes = m_pRenderer->GetDirect3D()->D3D11CreateVertexShader(pVertexBlob->GetBufferPointer(), pVertexBlob->GetBufferSize(), NULL, &m_pD3D11VertexShader);
		if (FAILED(hRes))
		{
			std::wstringstream wssOut;
			wssOut << "Couldn't create vertex shader \"" << m_wssShaderPath.str().c_str() << "\"";
			MessageBoxW(NULL, wssOut.str().c_str(), L"FAILED TO CREATE VERTEX SHADER !", MB_OK);
		}

		ID3D10Blob * pPixelBlob = nullptr;
		hRes = D3DCompileFromFile(m_wssShaderPath.str().c_str(), NULL, NULL, "psMain", "ps_5_0", D3D10_SHADER_ENABLE_STRICTNESS, 0, &pPixelBlob, &pErrorBlob);
		if (FAILED(hRes))
		{
			char* pCompErrors = nullptr;
			UINT iBuffSize = 0;
			std::stringstream ssOut;
			// Get a pointer to the error message text buffer.
			pCompErrors = (char*)(pErrorBlob->GetBufferPointer());

			// Get the length of the message.
			iBuffSize = pErrorBlob->GetBufferSize();

			// Write out the error message.
			for (UINT i = 0; i< iBuffSize; i++)
			{
				ssOut << pCompErrors[i];
			}

			OutputDebugStringA(ssOut.str().c_str());

			// Release the error message.
			pErrorBlob->Release();
			pErrorBlob = nullptr;

			pVertexBlob->Release();
			pVertexBlob = nullptr;

			return false;
		}

		hRes = m_pRenderer->GetDirect3D()->D3D11CreatePixelShader(pPixelBlob->GetBufferPointer(), pPixelBlob->GetBufferSize(), NULL, &m_pD3D11PixelShader);
		if (FAILED(hRes))
		{
			std::wstringstream wssOut;
			wssOut << "Couldn't create pixel shader \"" << m_wssShaderPath.str().c_str() << "\"";
			MessageBoxW(NULL, wssOut.str().c_str(), L"FAILED TO CREATE PIXEL SHADER !", MB_OK);
		}

		pPixelBlob->Release();
		pPixelBlob = nullptr;

		D3D11_INPUT_ELEMENT_DESC vertexLayout[] =
		{
			{ "POS", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
			{ "COLOUR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		};

		hRes = m_pRenderer->GetDirect3D()->D3D11CreateInputLayout(vertexLayout, 3, pVertexBlob->GetBufferPointer(), pVertexBlob->GetBufferSize(), &m_pD3D11InputLayout);
		if (FAILED(hRes))
		{
			return false;
		}

		pVertexBlob->Release();
		pVertexBlob = nullptr;

		D3D11_BUFFER_DESC constBufferDesc; ZeroMemory(&constBufferDesc, sizeof(constBufferDesc));
		constBufferDesc.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
		constBufferDesc.ByteWidth = sizeof(GlobalDefs::MatrixBuffer);
		constBufferDesc.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
		constBufferDesc.Usage = D3D11_USAGE_DYNAMIC;

		hRes = m_pRenderer->GetDirect3D()->D3D11CreateBuffer(&constBufferDesc, NULL, &m_pD3D11ConstantBuffer);
		if (FAILED(hRes))
			return false;

		m_bCreated = true;
		return IShader::Create();
	}

	//Its already created so may as well return true ?
	return true;
}

void CBasicShader::Update()
{
	return IShader::Update();
}

void CBasicShader::Draw()
{
	return IShader::Draw();
}

void CBasicShader::Destroy()
{


	//Shutdown of inherited objects done in base class
	return IShader::Destroy();
}

void CBasicShader::UpdateConstantBuffers(XMFLOAT4X4 xmf4x4World, XMFLOAT4X4 xmf4x4View, XMFLOAT4X4 xmf4x4Proj)
{
	D3D11_MAPPED_SUBRESOURCE mappedResource;
	m_pRenderer->GetDirect3D()->D3D11Map(m_pD3D11ConstantBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &mappedResource);
	GlobalDefs::MatrixBuffer * pConstBuff = reinterpret_cast<GlobalDefs::MatrixBuffer *> (mappedResource.pData);

	XMStoreFloat4x4(&pConstBuff->xmf4x4World, XMMatrixTranspose(XMLoadFloat4x4(&xmf4x4World)));
	XMStoreFloat4x4(&pConstBuff->xmf4x4View, XMMatrixTranspose(XMLoadFloat4x4(&xmf4x4View)));
	XMStoreFloat4x4(&pConstBuff->xmf4x4Proj, XMMatrixTranspose(XMLoadFloat4x4(&xmf4x4Proj)));

	m_pRenderer->GetDirect3D()->D3D11Unmap(m_pD3D11ConstantBuffer, 0);

	return IShader::UpdateConstantBuffers(xmf4x4World, xmf4x4View, xmf4x4Proj);
}