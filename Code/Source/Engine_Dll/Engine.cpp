#include <Include\Engine_Dll\stdafx.h>
#include <Include\Engine_Dll\Engine.h>

CEngine::CEngine():
m_bIsInitialized(false),
m_hMainWnd(NULL),
m_pSystemTimer(nullptr),
m_pSystemSettings(nullptr),
m_pMainConsole(nullptr),
m_pRenderer(nullptr),
m_pConsoleFunctions(nullptr)
{
	return;
}

CEngine::CEngine(HWND hMainWnd, CTimer * pTimer, CSettings * pSettings, CConsole * pConsole):
m_bIsInitialized(false),
m_hMainWnd(hMainWnd),
m_pSystemTimer(pTimer),
m_pSystemSettings(pSettings),
m_pMainConsole(pConsole),
m_pRenderer(nullptr),
m_pConsoleFunctions(nullptr)
{
	return;
}

CEngine::~CEngine()
{
	return;
}

bool CEngine::Init()
{
	if (!m_bIsInitialized)
	{
		m_pRenderer = new CRenderer(m_hMainWnd, m_pSystemTimer, m_pSystemSettings, m_pMainConsole);
		m_pRenderer->Init(m_hMainWnd);

		m_pConsoleFunctions = CConsoleFunctions::GetInstance(m_hMainWnd, m_pMainConsole, m_pRenderer);
		if (!m_pConsoleFunctions->RegisterFunctions())
			return false;
			//////////////////
		m_pBsSh = new CBasicShader(m_pRenderer);
		if (!m_pBsSh->Create())
			return false;

		m_pTC = new CEngineCamera(m_hMainWnd, m_pSystemTimer, m_pSystemSettings, m_pMainConsole);
		if (!m_pTC->Create())
			return false;
		m_pRenderer->GetDirect3D()->BindCamera(m_pTC);

		/*CBox * pBox = new CBox(m_pRenderer, m_pBsSh, XMFLOAT4(0,32,3,1));
		pBox->Create();
		m_pRenderer->GetDirect3D()->RegisterGeometry(pBox);*/

		std::stringstream ssPath;
		ssPath << GetRootFolderPath() << "Geometry\\Terrain\\Example\\\0";
		m_pTerrain = new CTerrain("Example", ssPath.str(), m_pRenderer, m_pBsSh);
		if (!m_pTerrain->Create())
			return false;
		m_pRenderer->GetDirect3D()->RegisterGeometry(m_pTerrain);
		////////////////////////

		m_bIsInitialized = true;
	}

	return true;
}

void CEngine::Update()
{
	m_pTC->Update();
	m_pBsSh->Update();

	m_pRenderer->Update();
	m_pRenderer->Draw();

	return;
}

void CEngine::Resize()
{
	m_pRenderer->Resize();

	return;
}

void CEngine::Shutdown()
{
	///////////////////////////
	m_pTC->Shutdown();
	SafeDelete(m_pTC);
	m_pBsSh->Destroy();
	SafeDelete(m_pBsSh);
	m_pTerrain->Destroy();
	SafeDelete(m_pTerrain);
	////////////////////
	m_pRenderer->Shutdown();
	SafeDelete(m_pRenderer);

	return;
}