XCOPY "%~dp0\..\Bin64\*.dll" "%~dp0\..\Game_Engine_Export\Bin64\" /Y
XCOPY "%~dp0\..\Bin64\*.exe" "%~dp0\..\Game_Engine_Export\Bin64\" /Y
XCOPY "%~dp0\..\Bin64\Libs\*.lib" "%~dp0\..\Game_Engine_Export\Bin64\Libs\" /Y
XCOPY "%~dp0\..\Settings\*.settings" "%~dp0\..\Game_Engine_Export\Settings\" /Y
XCOPY "%~dp0\..\Shaders\*.hlsl" "%~dp0\..\Game_Engine_Export\Shaders\" /Y
XCOPY "%~dp0\..\Geometry\*" "%~dp0\..\Game_Engine_Export\Geometry\" /Y /E