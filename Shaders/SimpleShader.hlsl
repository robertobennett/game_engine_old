//////////////
// TYPEDEFS //
//////////////
struct VertexInputType
{
    float3 position : POS;
    float3 norm : NORMAL;
	float4 colour : COLOUR;
};

struct PixelOutType
{
	float4 position : SV_POSITION;
	float4 colour : COLOUR;
};

cbuffer ConstBuffer
{
	matrix world;
	matrix view;
	matrix proj;
};

////////////////////////////////////////////////////////////////////////////////
// Vertex Shader
////////////////////////////////////////////////////////////////////////////////
PixelOutType vsMain(VertexInputType vIn)
{
	PixelOutType pOut;
	pOut.position = mul(float4(vIn.position, 1.0f), world);
	pOut.position = mul(pOut.position, view);
	pOut.position = mul(pOut.position, proj);

	float3 sunDir = float3(0, -50, 50);
	float4 diffColour = float4(1, 1, 1, 1);

	vIn.norm = mul(vIn.norm, (float3x3)world);

	sunDir = normalize(sunDir);
	vIn.norm = normalize(vIn.norm);

	float intensity = saturate(dot(vIn.norm, sunDir));

	pOut.colour = diffColour * (vIn.colour * intensity);

	return pOut;
}

////////////////////////////////////////////////////////////////////////////////
// Pixel Shader
////////////////////////////////////////////////////////////////////////////////
float4 psMain(PixelOutType pIn) : SV_TARGET
{
	return pIn.colour;
}